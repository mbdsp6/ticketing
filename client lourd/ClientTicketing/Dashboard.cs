﻿using ClientTicketing.Service;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ClientTicketing
{
    public partial class Dashboard : Form
    {
        private dynamic json;

        public Dashboard()
        {
            InitializeComponent();
            init();
        }

        private async void init()
        {
            var response = "";
            var form = new MultipartFormDataContent();
            var url = GlobalConfig.urlbase + "/evenement/eventuser/1";
            using (var client = new HttpClient())
            {
                this.eventdatagrid.Rows.Clear();
                response = await client.GetStringAsync(url);
                json = JsonConvert.DeserializeObject(response);
                foreach (var element in json)
                {
                    DataGridViewRow row = new DataGridViewRow();
                    row.CreateCells(this.eventdatagrid);
                    row.Cells[0].Value = element.idevenement;
                    row.Cells[1].Value = element.libelle;
                    row.Cells[2].Value = element.datefin;
                    row.Cells[3].Value = element.etat.libelle;

                    this.eventdatagrid.Rows.Add(row);
                }
            }
        }

        private void tabPage2_Click(object sender, EventArgs e)
        {

        }

        private void tabPage1_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void label7_Click(object sender, EventArgs e)
        {

        }

        private void refresh_Click(object sender, EventArgs e)
        {
            init();
        }

        private async void eventdatagrid_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            
            var response = "";
            var url = GlobalConfig.urlbase + "/evenement/event/"+ json[e.RowIndex].idevenement;
            using (var client = new HttpClient())
            {
                this.detailsgridview.Rows.Clear();
                response = await client.GetStringAsync(url);
                dynamic detailjson = JsonConvert.DeserializeObject(response);
                Console.WriteLine(detailjson);
                this.eventlabel.Text = detailjson.libelle;
                this.billetslabel.Text = detailjson.nombrebillet;
                this.billetsvenduslabel.Text = detailjson.nombreTicketVendu;
                this.billetsrestantslabel.Text = detailjson.nombreTicketRestant;
                foreach (var element in detailjson.categories)
                {
                    DataGridViewRow row = new DataGridViewRow();
                    row.CreateCells(this.detailsgridview);
                    row.Cells[0].Value = element.libelle;
                    row.Cells[1].Value = element.nombrebillet;
                    row.Cells[2].Value = element.nombreticketventdu;
                    row.Cells[3].Value = element.nombreticketdisponible;

                    this.detailsgridview.Rows.Add(row);
                }
            }
        }

        private void label8_Click(object sender, EventArgs e)
        {

        }
    }
}
