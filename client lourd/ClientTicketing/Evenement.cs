﻿using ClientTicketing.Model;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Web.Script.Serialization;
using System.Net.Http;
using System.IO;
using System.Net.Http.Headers;
using System.Text.RegularExpressions;
using ClientTicketing.Service;
using ClientTicketing.Utilitaire;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace ClientTicketing
{
    internal partial class evenement : Form
    {
        public evenement()
        {
            InitializeComponent();
        }

        private void dateTimePicker2_ValueChanged(object sender, EventArgs e)
        {
            heuredebut.Format = DateTimePickerFormat.Time;
            heuredebut.CustomFormat = "HH:MM";
        }

        private void quitterToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
        

        private void button3_Click(object sender, EventArgs e)
        {
            libelle.Clear();
            lieu.Clear();
            dateDebut.ResetText();
            heuredebut.ResetText();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog1 = new OpenFileDialog
            {
                InitialDirectory = @"D:\",
                Title = "Séléctionner votre fichier",

                CheckFileExists = true,
                CheckPathExists = true,

                DefaultExt = "jpg",
                Filter = "Image(*.BMP; *.JPG; *.JPEG; *.PNG)| *.BMP; *.JPG; *.JPEG; *.PNG | All files(*.*) | *.*",
                FilterIndex = 2,
                RestoreDirectory = true,

                ReadOnlyChecked = true,
                ShowReadOnly = true
            };

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                image.Text = openFileDialog1.FileName;
            }
        }

        private void nouveauToolStripMenuItem_Click(object sender, EventArgs e)
        {
            
        }

        private void enregistrer(object sender, EventArgs e)
        {

        }

        private void menuStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void button5_Click(object sender, EventArgs e)
        {

        }

        private void nouveauToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            
        }
        private void quitterToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void newToolStripMenuItem_Click(object sender, EventArgs e)
        {
            libelle.Clear();
            lieu.Clear();
            dateDebut.ResetText();
            heuredebut.ResetText();
        }

        private void ajouterLigneCategorie(object sender, EventArgs e)
        {
            Categorie categorie = new Categorie();


            categorieBindingSource.Add(categorie);
        }

        private void categorieGridView_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            Categorie categorie = new Categorie();
            categorie = (Categorie)this.categorieGridView.Rows[e.RowIndex].DataBoundItem;
            if (categorie!=null)
            {
            }
            
        }

        private void label8_Click(object sender, EventArgs e)
        {

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private async void button2_Click(object sender, EventArgs e)
        {
            Evenement evenement = new Evenement();
            evenement.libelle = this.libelle.Text;
            evenement.description = this.description.Text;
            evenement.nombrebillet = (int)this.nombrebillet.Value;
            evenement.datedebut = Utilitaire.Utilitaire.convertToDateTime(this.dateDebut.Value,this.heuredebut.Value);
            evenement.datefin = Utilitaire.Utilitaire.convertToDateTime(this.datefin.Value, this.heurefin.Value);
            evenement.lieu = this.lieu.Text;
            evenement.image = this.image.Text;
            evenement.categories = (IList<Categorie>)this.categorieBindingSource.List;
            evenement.etat = new Etat
            {
                idetat = 1
            };
            evenement.utilisateur = new Utilisateur
            {
                iduser = 1
            };
            EvenementService evenementService = new EvenementService();
            using (var response = await evenementService.SaveAsync(evenement, this.image.Text))
            {
                if (response.StatusCode==System.Net.HttpStatusCode.OK)
                {
                    MessageBox.Show("Evenement created successfully", "Message", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                   
                    var responseContent = await response.Content.ReadAsStringAsync();
                    JObject result = JObject.Parse(responseContent);
                    MessageBox.Show(result["message"].ToString(), "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }           
        }

        private void description_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Control==true && e.KeyCode == Keys.V)
            {             
                this.description.Text += (string)Clipboard.GetData("Text");
                e.SuppressKeyPress = true;
            }
        }

        private void description_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Control == true && e.KeyCode == Keys.V)
            {               
                this.description.Text += (string)Clipboard.GetData("Text");
                e.SuppressKeyPress = true;
            }
        }

        private void dashboardLinkLabel_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Dashboard db = new Dashboard();
            db.Show();
        }
    }
}
