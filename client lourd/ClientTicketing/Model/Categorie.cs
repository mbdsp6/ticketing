﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClientTicketing.Model
{
    class Categorie
    {
        public long idcategorie { get; set; }
        public string libelle { get; set; }
        public Double prix { get; set; }
        public int nombrebillet { get; set; }
    }
}
