﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClientTicketing.Model
{
    class Evenement
    {
        public long idevenement { get; set; }
        public string libelle { get; set; }
        public string description { get; set; }
        public DateTime datedebut { get; set; }
        public DateTime datefin { get; set; }
        public int nombrebillet { get; set; }
        public string lieu { get; set; }
        public string image { get; set; }

        public byte[] imagebyte { get; set; }

        public IList<Categorie> categories { get; set; }

        public Etat etat { get; set; }

        public Utilisateur utilisateur { get; set; }


    }
}
