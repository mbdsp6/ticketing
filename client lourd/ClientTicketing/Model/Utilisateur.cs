﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClientTicketing.Model
{
    class Utilisateur
    {
        public long iduser { get; set; }
        public string nom { get; set; }
        public string username { get; set; }
        public string password { get; set; }
    }
}
