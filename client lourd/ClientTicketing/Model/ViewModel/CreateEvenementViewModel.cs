﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace ClientTicketing.Model.ViewModel
{
    class CreateEvenementViewModel
    {
        public MultipartFormDataContent image { get; set; }
        public string evenement { get; set; }
    }
}
