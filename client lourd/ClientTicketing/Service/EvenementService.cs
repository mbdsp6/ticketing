﻿using ClientTicketing.Model;
using ClientTicketing.Model.ViewModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace ClientTicketing.Service
{
    class EvenementService : IEvenementService
    {
        public void save(Evenement evenement)
        {
            throw new NotImplementedException();
        }

        public void save(CreateEvenementViewModel evenement)
        {
            throw new NotImplementedException();
        }

        public async Task<HttpResponseMessage> SaveAsync(Evenement evenement, string imagepath)
        {
            
            var url = GlobalConfig.urlbase+"/evenement/create";
            //serealize evenement object
            var json = new JavaScriptSerializer().Serialize(evenement);
            //make new Date($1) to $1 
            json = Regex.Replace(json, @"\""\\/Date\((-?\d+)\)\\/\""", "$1");
            var form = new MultipartFormDataContent();
            var image = new ByteArrayContent(File.ReadAllBytes(imagepath));
            image.Headers.ContentType = MediaTypeHeaderValue.Parse("multipart/form-data");
            form.Add(image, "image", Path.GetFileName(imagepath));
            form.Add(new StringContent(json), "evenement");
            var response = new HttpResponseMessage() ;
            using (var client = new HttpClient())
            {
                 response= await client.PostAsync(url, form).ConfigureAwait(false);
            }
            return response;
        }
    }
}
