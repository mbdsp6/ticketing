﻿using ClientTicketing.Model;
using ClientTicketing.Model.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace ClientTicketing.Service
{
    interface IEvenementService
    {
        void save(Evenement evenement);

        void save(CreateEvenementViewModel evenement);

        Task<HttpResponseMessage> SaveAsync(Evenement evenement, string image);
    }
}
