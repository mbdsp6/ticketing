﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClientTicketing.Utilitaire
{
    class Utilitaire
    {
        public static DateTime convertToDateTime(DateTime date, DateTime hour)
        {
            return new DateTime(date.Year, date.Month, date.Day, hour.Hour, hour.Minute, 0);
        }
    }
}
