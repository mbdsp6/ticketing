const db = require("../models");
const UserAmount = db.useramount;

exports.create = (req, res) => {
    if (!req.body.user) {
        res.status(400).send({ message: "Content can not be empty!" });
        return;
    }

    // Create a Tutorial
    const useramount = new UserAmount({
        user: req.body.user,
        amount: req.body.amount,
    });

    // Save Tutorial in the database
    useramount
        .save(useramount)
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while creating the Tutorial."
            });
        });
};

exports.findOne = (req, res) => {
    const user = req.params.user;
    console.log(user);
    var condition = user ? { user: user } : {};

    UserAmount.find(condition)
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while retrieving tutorials."
            });
        });
};

exports.find = (req, res) => {
    var condition = {};

    UserAmount.find(condition)
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while retrieving tutorials."
            });
        });
};

exports.update = (req, res) => {
    if (!req.body) {
        return res.status(400).send({
          message: "Data to update can not be empty!"
        });
      }
    
      const user = req.params.user;
      const amount = req.params.amount;
    
    UserAmount.findOneAndUpdate({user: user},{$inc : {amount : amount}}, {})
        .then(data => {
            if (!data) {
                res.status(404).send({
                    message: `Cannot update UserAmount`
                });
            } else res.send({ message: "Compte credite avec succes" });
        })
        .catch(err => {
            res.status(500).send({
                message: "Error updating User Amount"
            });
        });
};

exports.decrease = (req, res) => {
    if (!req.body) {
        return res.status(400).send({
          message: "Data to update can not be empty!"
        });
      }
    
      const user = req.params.user;
      const amount = req.params.amount*(-1);
    
    UserAmount.findOneAndUpdate({user: user},{$inc : {amount : amount}}, {})
        .then(data => {
            if (!data) {
                res.status(404).send({
                    message: `Cannot update UserAmount`
                });
            } else res.send({ message: "Compte debite avec succes." });
        })
        .catch(err => {
            res.status(500).send({
                message: "Error updating UserAMount"
            });
        });
};