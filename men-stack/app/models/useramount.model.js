module.exports = mongoose => {
    var schema = mongoose.Schema(
        {
          user: Number,
          amount: Number
        }
    );
  
    schema.method("toJSON", function () {
        const { _id, user, amount, ...object } = this.toObject();
        object.id = _id;
        object.user = user;
        object.amount = amount;
        return object;
    });

    const Tutorial = mongoose.model("useramount", schema);
    return Tutorial;
};