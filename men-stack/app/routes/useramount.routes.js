module.exports = app => {
    const useramount = require("../controllers/useramount.controller.js");

    var router = require("express").Router();

    // Create a new Tutorial
    router.post("/", useramount.create);

    // Retrieve a single Tutorial with id
    router.get("/:user", useramount.findOne);

    router.get("/", useramount.find);

    // Update a Tutorial with id
    router.get("/crediter/:user/:amount", useramount.update);

    router.get("/debiter/:user/:amount", useramount.decrease);

    app.use('/api/useramount', router);
};