import React from 'react';
import { Alert, View, Text, StyleSheet, Image, TouchableOpacity } from 'react-native';

const CustomRow = ({ id, title, description, image_url, navigate, nextStep }) => (
    <TouchableOpacity
        style={styles.SubmitButtonStyle}
        activeOpacity={0}
        onPress={() => navigateToDetails(navigate, id, nextStep)}
    >
        <View
            style={styles.container}
            // onPress={(e) => onPressItem(title)}
            // onStartShouldSetResponder={() => navigateToDetails(navigate, id, nextStep)}
        >
            {/* <Image source={{ uri: image_url }} style={styles.photo} /> */}
            {/* <Image source={ require('./../../images/ticket.png') } style={styles.photo} /> */}
            <Image
                style={styles.mainimage}
                source={{ uri: "data:image/jpeg;base64," + image_url }} />
            <View style={styles.container_text}>
                <Text style={styles.title}>
                    {title}
                </Text>
                <Text style={styles.description}>
                    {description.substring(0, 100) + "..."}
                </Text>
            </View>

        </View>
    </TouchableOpacity>
);

function navigateToDetails(navigate, id, nextStep){
    navigate.navigate(
        nextStep,
        {id}
    );
}

function onPressItem(id){
    alert(id);
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'row',
        padding: 10,
        marginLeft:16,
        marginRight:16,
        marginTop: 8,
        marginBottom: 8,
        borderRadius: 5,
        backgroundColor: '#FFF',
        elevation: 2,
    },
    title: {
        fontSize: 16,
        color: '#000',
    },
    container_text: {
        flex: 1,
        flexDirection: 'column',
        marginLeft: 12,
        justifyContent: 'center',
    },
    description: {
        fontSize: 11,
        fontStyle: 'italic',
    },
    photo: {
        height: 50,
        width: 50,
    },
    mainimage: {
        width: 50,
        height: 50,
    },
});

export default CustomRow;