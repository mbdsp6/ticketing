import React from "react";

import { createDrawerNavigator } from "@react-navigation/drawer";

import { Homestack } from "../stacks/Homestack";
import { MyTicketStack } from "../stacks/MyTicketStack";
import { LoginStack } from "../stacks/LoginStack";

const Drawer = createDrawerNavigator();

const DrawerNavigator = () => {
  return (
    <Drawer.Navigator>
      <Drawer.Screen name="Acueil" component={Homestack} />
      <Drawer.Screen name="Mes tickets" component={MyTicketStack} />
      <Drawer.Screen name="Login" component={LoginStack} />
    </Drawer.Navigator>
  );
}

export default DrawerNavigator;