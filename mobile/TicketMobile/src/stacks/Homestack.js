import React from "react";
import { createStackNavigator } from "@react-navigation/stack";

import Home from "../views/Home";
import Detail from "../views/Detail";

const Stack = createStackNavigator();

const Homestack = () => {
  return (
      <Stack.Navigator
          screenOptions={{
              headerStyle: {
                  backgroundColor: "#9AC4F8",
              },
              headerTintColor: "white",
              headerBackTitle: "Back",
          }}
      >
          <Stack.Screen name="Acueil" component={Home} />
          <Stack.Screen name="Detail" component={Detail} />
      </Stack.Navigator>
  );
}

export { Homestack };