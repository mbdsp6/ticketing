import React from "react";
import { createStackNavigator } from "@react-navigation/stack";

import Login from "../views/Login";
import Home from "../views/Home";

const Stack = createStackNavigator();

const LoginStack = () => {
  return (
      <Stack.Navigator
          screenOptions={{
              headerStyle: {
                  backgroundColor: "#9AC4F8",
              },
              headerTintColor: "white",
              headerBackTitle: "Back",
          }}
      >
          <Stack.Screen name="Login" component={Login} />
          <Stack.Screen name="Accueil" component={Home} />
      </Stack.Navigator>
  );
}

export { LoginStack }; 