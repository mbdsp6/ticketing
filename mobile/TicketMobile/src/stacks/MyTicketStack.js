import React from "react";
import { createStackNavigator } from "@react-navigation/stack";

import MyTickets from "../views/MyTickets";
import QRCodeGenerate from "../views/QRCodeGenerate";

const Stack = createStackNavigator();

const MyTicketStack = () => {
  return (
      <Stack.Navigator
          screenOptions={{
              headerStyle: {
                  backgroundColor: "#9AC4F8",
              },
              headerTintColor: "white",
              headerBackTitle: "Back",
          }}
      >
          <Stack.Screen name="Mes tickets" component={MyTickets} />
          <Stack.Screen name="Billet" component={QRCodeGenerate} />
      </Stack.Navigator>
  );
}

export { MyTicketStack };