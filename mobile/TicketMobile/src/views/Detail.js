/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */
import 'react-native-gesture-handler';
import React, {Component} from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  Image,
  TouchableOpacity,
  TextInput,
  AsyncStorage
} from 'react-native';
import {Dropdown} from "react-native-material-dropdown-v2";
import {NumberEditor} from 'react-number-editor';
import {
    Colors,
  } from 'react-native/Libraries/NewAppScreen';
import axios from 'axios';

export default class Detail extends Component {
    constructor() {
        super();
    }

    state = {
        detail: {},
        loading: true,
        idcategorie: 0,
        ticketnumber: 0,
        iduser: 0,
    }

    bodyparams = {
        "iduser" : 1,
        "idevenement" : 1,
        "categories" : [
            {
                "idcategorie" : 1,
                "nombrebillet" : 1
            },
            {
                "idcategorie" : 2,
                "nombrebillet" : 1
            }
        ]
    }

    setSelectedStateValue = (idcat) =>{
        this.setState({
            idcategorie:idcat
        })
    }

    setNumber = (number) =>{
        this.setState({
            ticketnumber: number
        })
    }

    async init(){
        await AsyncStorage.getItem('loggedIn').then((value) => {
            if (value) {
                this.setState({
                    iduser: value
                })
            } else {
                alert("Not connected");
            }
        });
        var config = require('./../shared/global.json');
        var url = config.localurl + "evenement/event/"+this.props.route.params.id;
        const apicall = await fetch(url);
        const detail = await apicall.json();
        this.setState({ detail: detail, loading: false });
    }

    async componentDidMount(){
        try {
            this.init();
        } catch (err) {
            console.log("Error fetching data-----------", err);
        }
    }

    onChanged(text) {
        let newText = '';
        let numbers = '0123456789';

        for (var i = 0; i < text.length; i++) {
            if (numbers.indexOf(text[i]) > -1) {
                newText = newText + text[i];
            }
            else {
                // your call back function
                alert("please enter numbers only");
            }
        }
        this.setNumber(text);
    }

    onPressSubmit(event) {
        var params;
        params = {
            iduser : this.state.iduser,
            idevenement : this.props.route.params.id,
            categories : [
                {
                    "idcategorie" : this.state.idcategorie,
                    "nombrebillet" : this.state.ticketnumber
                }
            ]
        }
        var config = require('./../shared/global.json');
        var url = config.localurl + "/tickets/acheterticket/";
        console.log(url);
        console.log(JSON.stringify(params));
        axios.post(url, params)
            .then(res => {
                alert("achat effectué avec succes.");
                this.init();
            })
            .catch (error => {
                alert(error.response.data.message);
            });
    }

    render() {
        if(!this.state.loading){
            var dateevent = new Date(this.state.detail.datefin);
            var cats = this.state.detail.categories;
            var list = [];
            cats.forEach(element => {
                var newobject = {label: element.libelle, value: element.idcategorie}
                list.push(newobject)
            });
            var categ = this.state.detail.categories.find(product => product.idcategorie === this.state.idcategorie);
            return (
                <View style={styles.container}>
                    <SafeAreaView>
                        <ScrollView
                            contentInsetAdjustmentBehavior="automatic"
                            style={styles.scrollView}>
                            <View style={styles.logo}>
                                <Image 
                                    style={styles.mainimage} 
                                    source={{uri: "data:image/jpeg;base64,"+this.state.detail.imagebyte}}/>
                            </View>
                            <View style={styles.details}>
                                <Text style={styles.detailstext}><B>TItre: </B>{this.state.detail.libelle}</Text>
                                <Text style={styles.detailstext}><B>Description: </B>{this.state.detail.description}</Text>
                                <Text style={styles.detailstext}><B>Date de l'evenement: </B>{dateevent.toLocaleDateString("en-GB")}</Text>
                                <Text style={styles.detailstext}><B>Lieu: </B>{this.state.detail.lieu}</Text>
                                {   
                                    categ != null ?
                                    <>
                                        <Text style={styles.detailstext}><B>Prix unitaire: </B>{categ.prix} Ar</Text>
                                        <Text style={styles.detailstext}><B>Billet disponible: </B>{categ.nombreticketdisponible}</Text>
                                    </>
                                    :
                                    <></>
                                }
                            </View>
                            <View>
                                <Dropdown 
                                    data={list} 
                                    value={list.value} 
                                    label="Catégorie" 
                                    itemColor={'black'} 
                                    useNativeDriver={true} 
                                    onChangeText={(value,index,data)=>this.setSelectedStateValue(value)} />
                                <TextInput
                                    style={styles.textInput}
                                    keyboardType='number-pad'
                                    onChangeText={(text) => this.onChanged(text)}
                                    value={this.state.ticketnumber0}
                                /> 
                            </View>
                            <TouchableOpacity
                                style={styles.SubmitButtonStyle}
                                activeOpacity={.5}
                                onPress={(e) => this.onPressSubmit(e)}
                            >
                                <Text 
                                    style={styles.buttontext}
                                >
                                    Valider
                                </Text>
                            </TouchableOpacity>
                        </ScrollView>
                    </SafeAreaView>
                </View>
            );
        }else{
            return (<></>);
        }
    }
};

const B = (props) => <Text style={{fontWeight: 'bold'}}>{props.children}</Text>

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    scrollView: {
        backgroundColor: Colors.lighter,
    },
    logo: {
        marginTop: 50,
        justifyContent: 'center',
        alignItems: 'center',
    },
    details: {
        marginLeft: 10,
    },
    detailstext: {
    },
    SubmitButtonStyle: {
        marginTop: 10,
        paddingTop: 15,
        paddingBottom: 15,
        backgroundColor: '#00BCD4',
        borderRadius: 10,
        borderWidth: 1,
        borderColor: '#fff',
    },
    mainimage: {
        width: 220, 
        height: 100,
        marginBottom: 30,
    },
    buttontext: {
        justifyContent: 'center',
        alignSelf: "center",
        color: "#fff",
    }
});