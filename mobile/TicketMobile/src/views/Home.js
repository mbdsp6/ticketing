import 'react-native-gesture-handler';
import React, {Component} from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  FlatList,
  AsyncStorage,
  Alert,
  ActivityIndicator,
} from 'react-native';

import {
  Colors,
} from 'react-native/Libraries/NewAppScreen';

import CustomRow from './../shared/CustomRow';

export default class Home extends Component {
    constructor() {
        super();
    }

    state = {
        list: [],
        loading: true
    }

    async componentDidMount(){
        try {
            var config = require('./../shared/global.json');
            var url = config.localurl + "evenement/events";
            const apicall = await fetch(url);
            const list = await apicall.json();
            this.setState({ list: list, loading: false });
        } catch (err) {
            console.log("Error fetching data-----------", err);
        }
    }

    render() {
        const datas = this.state.list;
        const loading = this.state.loading;
        var nStep = 'Detail';
        AsyncStorage.getItem('loggedIn').then((value) => {
            if (value) {
                
            } else {
                
            }
        });
        if(!loading) {
            return (
                <View style={styles.container}>
                    <SafeAreaView>
                        <ScrollView
                            contentInsetAdjustmentBehavior="automatic"
                            style={styles.scrollView}>
                            <FlatList
                                data={datas}
                                renderItem={({ item }) => <CustomRow
                                    id={item.idevenement}
                                    title={item.libelle}
                                    description={item.description}
                                    image_url={item.imagebyte}
                                    navigate={this.props.navigation}
                                    nextStep={nStep}
                                />}
                            />
                        </ScrollView>
                    </SafeAreaView>
                </View>
            );
        }else {
            return <ActivityIndicator />
        }
    }

};

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    scrollView: {
        backgroundColor: Colors.lighter,
    },
    logo: {
        marginTop: 50,
        justifyContent: 'center',
        alignItems: 'center',
    },
    input: {
        height: 40,
        borderColor: 'gray',
        borderWidth: 1,
        marginTop: 10,
        marginBottom: 10,
        borderRadius: 10,
    },
    SubmitButtonStyle: {
        marginTop: 10,
        paddingTop: 10,
        paddingBottom: 10,
        backgroundColor: '#00BCD4',
        borderRadius: 10,
        borderWidth: 1,
        borderColor: '#fff'
    },
    TextStyle: {
        color: '#fff',
        textAlign: 'center',
    },
    body: {
        backgroundColor: Colors.white,
    },
    sectionContainer: {
        marginTop: 32,
        paddingHorizontal: 24,
    },
    sectionTitle: {
        fontSize: 24,
        fontWeight: '600',
        color: Colors.black,
        textAlign: 'center',
    },
    sectionDescription: {
        marginTop: 8,
        fontSize: 18,
        fontWeight: '400',
        color: Colors.dark,
    },
});