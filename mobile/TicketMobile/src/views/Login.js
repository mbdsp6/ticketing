/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */
import 'react-native-gesture-handler';
import React, {Component} from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  TextInput,
  TouchableOpacity,
  Image,
  Alert,
  AsyncStorage,
} from 'react-native';

import {
  Colors,
} from 'react-native/Libraries/NewAppScreen';

export default class Login extends Component {
    constructor() {
        super();
    }
    state = {
        username: '',
        password: ''
    };
    render() {
        return (
            <View style={styles.container}>
                <SafeAreaView>
                    <ScrollView
                        contentInsetAdjustmentBehavior="automatic"
                        style={styles.scrollView}>
                        <View style={styles.logo}>
                            <Image
                                source={require('./../../images/ticket.png')}
                            />
                        </View>
                        <View style={styles.body}>
                            <View style={styles.sectionContainer}>
                                <Text style={styles.sectionTitle}>Connectez-vous ici</Text>
                                <TextInput
                                    style={styles.input}
                                    onChangeText={(value) => this.setState({username: value})}
                                    value={this.state.username}
                                />
                                <TextInput
                                    style={styles.input}
                                    secureTextEntry={true}
                                    onChangeText={(value) => this.setState({password: value})}
                                    value={this.state.password}
                                />
                                <TouchableOpacity
                                    style={styles.SubmitButtonStyle}
                                    activeOpacity={.5}
                                    onPress={(e) => this.onPressConnect(e, 'test','test')}
                                >
                                    <Text style={styles.TextStyle}> CONNEXION </Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </ScrollView>
                </SafeAreaView>
            </View>
        );
    }

    onPressConnect(event, login, password) {
        var config = require('./../shared/global.json');
        var url = config.localurl + "utilisateur/seconnecter";
        fetch(url,{
            method: 'POST',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
            },
            body: "username="+this.state.username+"&password="+this.state.password
        })
            .then(response => response.json())
            .then((responseJson) => {
                if(responseJson.status == null){
                    AsyncStorage.setItem('loggedIn', responseJson.iduser.toString());
                    const { navigate } = this.props.navigation; 
                    navigate('Accueil');
                }else{
                    Alert.alert("Erreur d'identification",responseJson.message);
                }
            })
            .catch(
                error => alert(error)
            ) 
    }

};

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    scrollView: {
        backgroundColor: Colors.lighter,
    },
    logo: {
        marginTop: 50,
        justifyContent: 'center',
        alignItems: 'center',
    },
    input: {
        height: 40,
        borderColor: 'gray',
        borderWidth: 1,
        marginTop: 10,
        marginBottom: 10,
        borderRadius: 10,
    },
    SubmitButtonStyle: {
        marginTop: 10,
        paddingTop: 10,
        paddingBottom: 10,
        backgroundColor: '#00BCD4',
        borderRadius: 10,
        borderWidth: 1,
        borderColor: '#fff'
    },
    TextStyle: {
        color: '#fff',
        textAlign: 'center',
    },
    body: {
        backgroundColor: Colors.white,
    },
    sectionContainer: {
        marginTop: 32,
        paddingHorizontal: 24,
    },
    sectionTitle: {
        fontSize: 24,
        fontWeight: '600',
        color: Colors.black,
        textAlign: 'center',
    },
    sectionDescription: {
        marginTop: 8,
        fontSize: 18,
        fontWeight: '400',
        color: Colors.dark,
    },
});