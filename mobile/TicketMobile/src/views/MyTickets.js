import 'react-native-gesture-handler';
import React, {Component} from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  FlatList,
  AsyncStorage,
  RefreshControl,
} from 'react-native';

import {
  Colors,
} from 'react-native/Libraries/NewAppScreen';

import CustomRow from './../shared/CustomRow';

export default class MyTickets extends Component {
    constructor() {
        super();
    }

    state = {
        list: [],
        loading: true,
        refreshing: false,
    }

    async init() {
        var config = require('./../shared/global.json');
        var url = config.localurl + "tickets/";
        await AsyncStorage.getItem('loggedIn').then((value) => {
            if (value) {
                url += value;
            } else {
                alert("Not connected");
            }
        });
        const apicall = await fetch(url);
        const list = await apicall.json();
        this.setState({ list: list, loading: false });
        this.setState({refreshing: false});
    }

    async componentDidMount(){
        try {
            this.init();
        } catch (err) {
            console.log("Error fetching data-----------", err);
        }
    }

    async onRefresh() {
        this.setState({refreshing: true});
        this.init();
    };

    render() {
        const datas = this.state.list;
        const loading = this.state.loading;
        var nStep = 'Billet';
        if(!loading) {
            return (
                <View style={styles.container}>
                    <SafeAreaView>
                        <ScrollView
                            contentInsetAdjustmentBehavior="automatic"
                            refreshControl={
                                <RefreshControl refreshing={this.state.refreshing} onRefresh={this.onRefresh} />
                            }
                            style={styles.scrollView}>
                            <FlatList
                                data={datas}
                                renderItem={({ item }) => <CustomRow
                                    id = {item.idticket}
                                    title={item.evenement.libelle+" ("+item.evenement.lieu+")"}
                                    description={item.categorie.libelle + "- "+item.categorie.prix+" Ar"}
                                    image_url={item.evenement.imagebyte}
                                    navigate={this.props.navigation}
                                    nextStep={nStep}
                                />}
                            />
                        </ScrollView>
                    </SafeAreaView>
                </View>
            );
        }else{
            return(<></>);
        }
    }

};

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    scrollView: {
        backgroundColor: Colors.lighter,
    },
    logo: {
        marginTop: 50,
        justifyContent: 'center',
        alignItems: 'center',
    },
    input: {
        height: 40,
        borderColor: 'gray',
        borderWidth: 1,
        marginTop: 10,
        marginBottom: 10,
        borderRadius: 10,
    },
    SubmitButtonStyle: {
        marginTop: 10,
        paddingTop: 10,
        paddingBottom: 10,
        backgroundColor: '#00BCD4',
        borderRadius: 10,
        borderWidth: 1,
        borderColor: '#fff'
    },
    TextStyle: {
        color: '#fff',
        textAlign: 'center',
    },
    body: {
        backgroundColor: Colors.white,
    },
    sectionContainer: {
        marginTop: 32,
        paddingHorizontal: 24,
    },
    sectionTitle: {
        fontSize: 24,
        fontWeight: '600',
        color: Colors.black,
        textAlign: 'center',
    },
    sectionDescription: {
        marginTop: 8,
        fontSize: 18,
        fontWeight: '400',
        color: Colors.dark,
    },
});