/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */
import 'react-native-gesture-handler';
import React, {Component} from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  Image,
} from 'react-native';

import {
    Colors,
  } from 'react-native/Libraries/NewAppScreen';

import QRCode from 'react-native-qrcode-svg';

export default class QRCodeGenerate extends Component {
    constructor() {
        super();
    }
    render() {
        var config = require('./../shared/global.json');
        var url = config.localurl + "checkTicket/"+this.props.route.params.id;
        return (
            <View style={styles.container}>
                <QRCode
                    value={url}
                    color={'#2C8DDB'}
                    backgroundColor={'white'}
                    size={300}
                    />
            </View>
        );
    }
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
});