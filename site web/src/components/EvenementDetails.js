import React, { Component } from "react";

import { withStyles, fade, makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import ButtonBase from '@material-ui/core/ButtonBase';
import metallicaData from './../data/mettalica';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import PropTypes from 'prop-types';
import history from './../history';
import "./EvenementDetails.css";
import EvenementService from "../services/EvenementService";
import TicketService from "../services/TicketService";
import Utils from "../utils/Utils";
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import TextField from '@material-ui/core/TextField';
import IconButton from '@material-ui/core/IconButton';
import AddCircleIcon from '@material-ui/icons/AddCircle';
import RemoveCircleIcon from '@material-ui/icons/RemoveCircle';
import Button from '@material-ui/core/Button';
import SnackBar from './SnackBar/SnackBar';


const useStyles = theme => ({
  root: {
    flexGrow: 1,
  },
  underline: {
    width: 100,
    "&&&:before": {
      borderBottom: "none"
    },
    "&&:after": {
      borderBottom: "none"
    },
    textAlign: "center"
  },
  paper: {
    padding: theme.spacing(1),
    margin: 'auto',
    maxWidth: 1000,
    height: 300,
  },
  description: {
    padding: theme.spacing(1),
    maxWidth: 1000,
    margin: 'auto',
    marginTop: 5,
    height: 'auto',
  },

  image: {
    width: 500,
    height: 300,
  },
  img: {
    margin: 'auto',
    display: 'block',
    maxWidth: '100%',
    maxHeight: '100%',
  },
  listTitre: {
    backgroundColor: '#fafafa',
    width: '50px'
  },
  listDesc: {
    width: '50px',
    textAlign: "left",
  },
  containerDesc: {
    paddingTop: "27px"
  }

});

class EvenementDetails extends Component {
  constructor(props) {
    super(props);
    this.getEvenement = this.getEvenement.bind(this);
    this.acheterTicket = this.acheterTicket.bind(this);
    this.totalTickets = this.totalTickets.bind(this);
    this.totalPrix = this.totalPrix.bind(this);
    this.setOpen = this.setOpen.bind(this);
    this.state = {
      currentEvenement: undefined,
      message: "",
      classes: "",
      totalTicket: 0,
      totalPrice: 0,
      open: false,
      color :"success"
    };
  }
  handleListItemClick = (event, url) => {
    history.push(url);
  };

  initNombreBillet(data) {
    data.categories.find(function (categorie, i) {
      categorie.nombrebillet = 0;
      categorie.nombreticketventdu = categorie.nombrebillet * categorie.prix;
    });
    return data;
  }
  handlePlusButton = (event, index) => {
    var currentEvent = this.state.currentEvenement;
    currentEvent.categories.find(function (categorie, i) {
      if (categorie.idcategorie == index) {
        categorie.nombrebillet = categorie.nombrebillet + 1;
        categorie.nombreticketventdu = categorie.nombrebillet * categorie.prix;
      }
    });
    this.totalTickets(currentEvent);
    this.totalPrix(currentEvent);
    this.setState({
      currentEvenement: currentEvent
    });
  }
  handleMinusButton = (event, index) => {
    var currentEvent = this.state.currentEvenement;
    currentEvent.categories.find(function (categorie, i) {
      if (categorie.idcategorie == index) {
        categorie.nombrebillet = categorie.nombrebillet - 1;
        categorie.nombreticketventdu = categorie.nombrebillet * categorie.prix;
      }
    });
    this.totalTickets(currentEvent);
    this.totalPrix(currentEvent);
    this.setState({
      currentEvenement: currentEvent
    });
  }

  handleChangeNombreBillet(event, index) {
    var currentEvent = this.state.currentEvenement;
    var value = event.target.value;
    currentEvent.categories.find(function (categorie, i) {
      if (categorie.idcategorie == index) {
        if (parseInt(value) == 0 || parseInt(value) == categorie.nombreticketdisponible) {
          event.target.disabled = true;
        }
        categorie.nombrebillet = parseInt(value);
        categorie.nombreticketventdu = categorie.nombrebillet * categorie.prix;
      }
    });
    this.totalTickets(currentEvent);
    this.totalPrix(currentEvent);
    this.setState({
      currentEvenement: currentEvent
    });
  }

  totalTickets(evenement) {
    var total = evenement.categories.map(({ nombrebillet }) => nombrebillet).reduce((sum, i) => sum + i, 0);
    this.setState({
      totalTicket: total
    });
  }

  totalPrix(evenement) {
    var total = evenement.categories.map(({ nombreticketventdu }) => nombreticketventdu).reduce((sum, i) => sum + i, 0);
    this.setState({
      totalPrice: total
    });
  }

  disableInput(index) {
    if (this.disableButtonMinus(index) && this.disableButtonPlus(index)) {
      return true;
    }
    return false;
  }
  disableButtonMinus(index) {
    var currentEvent = this.state.currentEvenement;
    var ret = false;
    currentEvent.categories.find(function (categorie, i) {
      if (categorie.idcategorie == index) {
        if (categorie.nombrebillet == 0) {
          ret = true;
        }
      }
    });
    return ret;
  }
  disableButtonPlus(index) {
    var currentEvent = this.state.currentEvenement;
    var ret = false;
    currentEvent.categories.find(function (categorie, i) {
      if (categorie.idcategorie == index) {
        if (categorie.nombrebillet == categorie.nombreticketdisponible) {
          ret = true;
        }
      }
    });
    return ret;
  }
  ccyFormat(num) {
    return `${num.toFixed(2)}`;
  }

  componentDidMount() {
    this.getEvenement(this.props.match.params.id);
  }
  getEvenement(id) {
    EvenementService.get(id)
      .then(response => {
        this.setState({
          currentEvenement: this.initNombreBillet(response.data)
        });
        // console.log(response.data);
      })
      .catch(e => {
        console.log(e);
      });
  }
  setOpen(open) {
    this.setState({
      open: open
    });
  }
  acheterTicket() {
    var data = {
      idevenement: this.state.currentEvenement.idevenement,
      iduser: 1,
      categories: this.state.currentEvenement.categories
    }
    TicketService.acheterTicket(data)
      .then(response => {
        if (response.status == 200) {
          this.getEvenement(this.props.match.params.id);
          this.setState({
            open: true,
            message: response.data,
            color :"success"
          });
        } 
      }).catch(e => {
        this.setState({
          open: true,
          message: e.response.data.message,
          color : "error"
        });
      });
  }

  render() {
    EvenementDetails.propTypes = {
      classes: PropTypes.object.isRequired,
    };
    const { classes } = this.props;
    const { currentEvenement } = this.state;

    return (
      <div className={classes.root}>
        {
          currentEvenement && (
            <div>
              <Paper className={classes.paper}>
                <Grid container spacing={2}>
                  <Grid item>
                    <ButtonBase className={classes.image}>
                      <img className={classes.img} alt={currentEvenement.image} src={"data:image/jpeg;base64," + currentEvenement.imagebyte} />
                    </ButtonBase>
                  </Grid>
                  <Grid item xs={12} sm container className={classes.containerDec}>
                    <Grid item xs container direction="column" spacing={2}>
                      <Grid item xs>
                        <List component="nav" aria-label="contacts">
                          <ListItem button>
                            <ListItemText className={classes.listTitre} primary="Evenement" />
                            <ListItemText className={classes.listDesc} primary={currentEvenement.libelle} />
                          </ListItem>
                          <ListItem button>
                            <ListItemText className={classes.listTitre} primary="Date début" />
                            <ListItemText onClick={event => this.handleListItemClick(event, "/")} className={classes.listDesc} primary={Utils.renderDateFormat(currentEvenement.datedebut)} />
                          </ListItem>
                          <ListItem button>
                            <ListItemText className={classes.listTitre} primary="Heure début" />
                            <ListItemText className={classes.listDesc} primary={Utils.renderTime(currentEvenement.datedebut)} />
                          </ListItem>
                          <ListItem button>
                            <ListItemText className={classes.listTitre} primary="Lieu" />
                            <ListItemText className={classes.listDesc} primary={currentEvenement.lieu} />
                          </ListItem>
                          <ListItem button>
                            <ListItemText className={classes.listTitre} primary="Nombre ticket restant" />
                            <ListItemText className={classes.listDesc} primary={currentEvenement.nombreTicketRestant} />
                          </ListItem>
                        </List>
                      </Grid>
                    </Grid>
                  </Grid>
                </Grid>
              </Paper>
              <Paper className={classes.description}>
                <Typography gutterBottom>
                  {currentEvenement.description}
                </Typography>
              </Paper>

              <TableContainer component={Paper} >
                <Table className={classes.table} aria-label="spanning table">
                  <TableHead>
                    <TableRow>
                      <TableCell align="center" colSpan={4}>Details</TableCell>
                      <TableCell align="right">Price</TableCell>
                    </TableRow>
                    <TableRow>
                      <TableCell component="th" >Libelle</TableCell>
                      <TableCell component="th" align="right" >Prix Unitaire</TableCell>
                      <TableCell component="th" align="right">Nombre billet disponible</TableCell>
                      <TableCell component="th" align="center">Quantité</TableCell>
                      <TableCell component="th" align="right">Prix</TableCell>
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {currentEvenement.categories.map(categorie => (
                      <TableRow key={categorie.idcategorie}>
                        <TableCell scope="row">{categorie.libelle}</TableCell>
                        <TableCell align="right">{this.ccyFormat(categorie.prix)}</TableCell>
                        <TableCell align="right">{categorie.nombreticketdisponible}</TableCell>
                        <TableCell align="center">
                          <IconButton aria-label="delete" className={classes.margin} size="medium"
                            onClick={event => this.handleMinusButton(event, categorie.idcategorie)}
                            disabled={this.disableButtonMinus(categorie.idcategorie)}
                          >
                            <RemoveCircleIcon fontSize="inherit" />
                          </IconButton>
                          <TextField
                            id={"ticketNumber_" + categorie.idcategorie}
                            name={"ticketNumber_" + categorie.idcategorie}
                            type="number"
                            value={categorie.nombrebillet}
                            className={classes.underline}
                            inputProps={{ style: { textAlign: 'center' } }}
                            disabled={this.disableInput(categorie.idcategorie)}
                            onChange={event => this.handleChangeNombreBillet(event, categorie.idcategorie)}
                          />
                          <IconButton aria-label="delete" className={classes.margin} size="medium"
                            onClick={event => this.handlePlusButton(event, categorie.idcategorie)}
                            disabled={this.disableButtonPlus(categorie.idcategorie)}
                          >
                            <AddCircleIcon fontSize="inherit" />
                          </IconButton>
                        </TableCell>
                        <TableCell align="right" >{this.ccyFormat(categorie.nombreticketventdu)}</TableCell>
                      </TableRow>
                    ))}
                    <TableRow>
                      <TableCell colSpan={2}></TableCell>
                      <TableCell align="right">Total</TableCell>
                      <TableCell align="center">{this.state.totalTicket}</TableCell>
                      <TableCell align="right">{this.ccyFormat(this.state.totalPrice)}</TableCell>
                    </TableRow>
                    <TableRow>
                      <TableCell colSpan={3}></TableCell>
                      <TableCell colSpan={2} align="right">
                        <Button onClick={this.acheterTicket} size="large" variant="contained" color="primary">
                          Reserver
                        </Button>
                      </TableCell>
                    </TableRow>
                  </TableBody>
                </Table>
              </TableContainer>
              <SnackBar open={this.state.open} color={this.state.color} message={this.state.message} closeNotification={this.setOpen.bind(this, false)} />

            </div>
          )
        }
      </div>
    );
  }
}
export default withStyles(useStyles)(EvenementDetails);