import React, { Component } from "react";
import EvenementService from "../services/EvenementService";
import Utils from "../utils/Utils";
import { Link } from "react-router-dom";
import { makeStyles, withStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import metallicaData from './../data/mettalica';
import history from './../history';
import PropTypes from 'prop-types';
import ButtonBase from '@material-ui/core/ButtonBase';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import Container from '@material-ui/core/Container';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import ListItemText from '@material-ui/core/ListItemText';
import FolderIcon from '@material-ui/icons/Folder';
import InboxIcon from '@material-ui/icons/Inbox';
import DraftsIcon from '@material-ui/icons/Drafts';
import CardHeader from '@material-ui/core/CardHeader';
import LocationOnRoundedIcon from '@material-ui/icons/LocationOnRounded';
import DateRangeRoundedIcon from '@material-ui/icons/DateRangeRounded';
import ScheduleRoundedIcon from '@material-ui/icons/ScheduleRounded';
const useStyles = makeStyles((theme) => ({
  table: {
    minWidth: 650,
  },
  media: {
    height: 400,
    paddingTop: '56.25%', // 16:9
  }
}));
export default class EvenementsList extends Component {
  constructor(props) {
    super(props);
    this.retrieveEvenements = this.retrieveEvenements.bind(this);
    this.refreshList = this.refreshList.bind(this);
    this.setActiveEvenement = this.setActiveEvenement.bind(this);
    this.removeAllEvenements = this.removeAllEvenements.bind(this);
    this.state = {
      evenements: [],
      currentEvenement: null,
      currentIndex: -1,
      classes: ""
      // searchTitle: ""
    };
  }
  componentDidMount() {
    this.retrieveEvenements();
  }
  retrieveEvenements() {
    EvenementService.getAll()
      .then(response => {
        this.setState({
          evenements: response.data
        });
        console.log(response.data);
      })
      .catch(e => {
        console.log(e);
      });
  }

  refreshList() {
    this.retrieveEvenements();
    this.setState({
      currentEvenement: null,
      currentIndex: -1
    });
  }

  setActiveEvenement(evenement, index) {
    this.setState({
      currentEvenement: evenement,
      currentIndex: index
    });
  }

  removeAllEvenements() {
    EvenementService.deleteAll()
      .then(response => {
        console.log(response.data);
        this.refreshList();
      })
      .catch(e => {
        console.log(e);
      });
  }

  handleClick = (event, url) => {
    history.push(url);
  };


  render() {
    EvenementsList.propTypes = {
      classes: PropTypes.object.isRequired,
    };
    const { evenements, currentEvenement, currentIndex, classes } = this.state;
    return (
      <Container >
        <Grid container spacing={3}>
          {evenements.map((evenement,i) => (
            <Grid key={i} item xs={4}>
              <Card className={classes.root}>
                <CardActionArea>
                  <CardMedia className={classes.media}
                    component="img"
                    alt={evenement.image}
                    height="200"
                    image={"data:image/jpeg;base64," + evenement.imagebyte}
                    title={evenement.image}
                  />
                  <CardContent>
                    <Typography gutterBottom variant="h5" component="h2">
                      {evenement.libelle}
                    </Typography>
                    <List component="nav" aria-label="main mailbox folders">
                      <ListItem button>
                        <ListItemIcon>
                          <DateRangeRoundedIcon />
                        </ListItemIcon>
                        <ListItemText>
                          {
                            Utils.renderDateFormat(evenement.datedebut)
                          }
                        </ListItemText>
                      </ListItem>
                      <ListItem button>
                        <ListItemIcon>
                          <ScheduleRoundedIcon />
                        </ListItemIcon>
                        <ListItemText primary={Utils.renderTime(evenement.datedebut)} />
                        <ListItemText primary={Utils.renderTime(evenement.datefin)} />
                      </ListItem>
                      <ListItem button>
                        <ListItemIcon>
                          <LocationOnRoundedIcon />
                        </ListItemIcon>
                        <ListItemText primary={evenement.lieu} />
                      </ListItem>
                    </List>
                    <Typography variant="body2" color="textSecondary" component="p">
                      {evenement.description.substring(0, 150)+"..."}
                    </Typography>
                  </CardContent>
                </CardActionArea>
                <CardActions>
                  <Button size="small" onClick={event => this.handleClick(event, "/evenement/" + evenement.idevenement)} variant="contained" color="primary">
                    Reserver
                  </Button>
                </CardActions>
              </Card>
            </Grid>

          ))}
        </Grid>
      </Container>

    );
  }
}