import React, { useState, useEffect } from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import UtilisateurService from '../services/UtilisateurService';

export default function LoginForm(props) {
    

    const [username, setUsername] = React.useState("");
    const [password, setPassword] = React.useState("");
   

    const handleClose = () =>{
        props.close();
    }
    const handleSubmit = () => {
        login();
        
    };

    const handleUsernameChange = (event)=>{
        setUsername(event.target.value)
    };
    const handlePasswordChange = (event)=>{
        setPassword(event.target.value)
    };
    
    const login = () => {
        var data = {
            username: username,
            password: password,
        }       
        UtilisateurService.login(data.username,data.password)
            .then((response) => {
                
                localStorage.setItem("userStorage",JSON.stringify(response.data));

                props.close();
            }).catch((error) => {
                console.log(error.data);
            });
    }

    return (
        <div>
            <Dialog open={props.open} onClose={handleClose} aria-labelledby="form-dialog-title">
                <DialogTitle id="form-dialog-title">Se connecter</DialogTitle>
                <DialogContent>

                    <TextField
                        value={username}
                        autoFocus
                        margin="dense"
                        id="username"
                        label="Username"
                        type="email"
                        fullWidth
                        onChange = {handleUsernameChange}
                    />
                    <TextField
                        autoFocus
                        value={password}
                        margin="dense"
                        id="password"
                        label="Mot de passe"
                        type="password"
                        fullWidth
                        onChange = {handlePasswordChange}
                    />
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleClose} color="primary">
                        Cancel
          </Button>
                    <Button onClick={handleSubmit} color="primary">
                        Subscribe
          </Button>
                </DialogActions>
            </Dialog>
        </div>
    );
}