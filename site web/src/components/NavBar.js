import React, { useState, useEffect } from 'react';
import { fade, makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import InputBase from '@material-ui/core/InputBase';
import Badge from '@material-ui/core/Badge';
import MenuItem from '@material-ui/core/MenuItem';
import Menu from '@material-ui/core/Menu';
import MenuIcon from '@material-ui/icons/Menu';
import SearchIcon from '@material-ui/icons/Search';
import AccountCircle from '@material-ui/icons/AccountCircle';
import MailIcon from '@material-ui/icons/Mail';
import NotificationsIcon from '@material-ui/icons/Notifications';
import MoreIcon from '@material-ui/icons/MoreVert';
import history from './../history';
import ButtonBase from '@material-ui/core/ButtonBase';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import ExitToAppOutlinedIcon from '@material-ui/icons/ExitToAppOutlined';
import MonetizationOnIcon from '@material-ui/icons/MonetizationOn';
import UtilisateurService from '../services/UtilisateurService';
import LoginForm from '../components/LoginForm';
const useStyles = makeStyles(theme => ({
  grow: {
    flexGrow: 1,
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  title: {
    display: 'none',
    [theme.breakpoints.up('sm')]: {
      display: 'block',
    },
  },
  search: {
    position: 'relative',
    borderRadius: theme.shape.borderRadius,
    backgroundColor: fade(theme.palette.common.white, 0.15),
    '&:hover': {
      backgroundColor: fade(theme.palette.common.white, 0.25),
    },
    marginRight: theme.spacing(2),
    marginLeft: 0,
    width: '100%',
    [theme.breakpoints.up('sm')]: {
      marginLeft: theme.spacing(3),
      width: 'auto',
    },
  },
  searchIcon: {
    padding: theme.spacing(0, 2),
    height: '100%',
    position: 'absolute',
    pointerEvents: 'none',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  inputRoot: {
    color: 'inherit',
  },
  inputInput: {
    padding: theme.spacing(1, 1, 1, 0),
    // vertical padding + font size from searchIcon
    paddingLeft: `calc(1em + ${theme.spacing(4)}px)`,
    transition: theme.transitions.create('width'),
    width: '100%',
    [theme.breakpoints.up('md')]: {
      width: '20ch',
    },
  },
  sectionDesktop: {
    display: 'none',
    [theme.breakpoints.up('md')]: {
      display: 'flex',
    },
  },
  sectionMobile: {
    display: 'flex',
    [theme.breakpoints.up('md')]: {
      display: 'none',
    },
  },
}));

export default function PrimarySearchAppBar() {
  const classes = useStyles();
  const [anchorEl, setAnchorEl] = React.useState(null);
  const [authenticated, setAuthenticated] = React.useState(false);
  const [mobileMoreAnchorEl, setMobileMoreAnchorEl] = React.useState(null);
  const [currentUser, setCurrentUser] = React.useState();
  const [ammount, setAmmount] = React.useState(0);

  const [open, setOpen] = React.useState(false);

  useEffect(() => {
    //  localStorage.removeItem("userStorage");   
    init();


  }, [])

  const handleClose = () => {
    setOpen(false);
    getAmount(currentUser.iduser);
    setAuthenticated(true);
  }


  const init = () => {
    var userStorage = JSON.parse(localStorage.getItem("userStorage"));
    if (!userStorage) {
      setAuthenticated(false);
    } else {
      setAuthenticated(true);
      setCurrentUser(userStorage);
      getAmount(userStorage.iduser);
    }
  }

  const isMenuOpen = Boolean(anchorEl);
  const isMobileMenuOpen = Boolean(mobileMoreAnchorEl);

  const handleProfileMenuOpen = event => {
    setAnchorEl(event.currentTarget);
  };

  const getAmount = (id) => {
    UtilisateurService.getUserAmount(id).then((response) => {
      setAmmount(response.data);
    }).catch((error) => {
      console.log(error);
    });
  };


  const handleMobileMenuClose = () => {
    setMobileMoreAnchorEl(null);
  };

  const handleMenuClose = () => {
    setAnchorEl(null);
    handleMobileMenuClose();
  };

  const openLoginButton = event => {
    setOpen(true);
  };

  const logOut = () => {
    UtilisateurService.logOut();
  };

  const handleMobileMenuOpen = event => {
    setMobileMoreAnchorEl(event.currentTarget);
  };

  const toHome = (event, url) => {
    history.push(url);
  }

  const navigateTo = (url) => {
    history.push(url);
  }

  const toPrevious = (event, url) => {
    history.goBack();
  }


  const menuId = 'primary-search-account-menu';
  const renderMenu = (
    <Menu
      anchorEl={anchorEl}
      anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
      id={menuId}
      keepMounted
      transformOrigin={{ vertical: 'top', horizontal: 'right' }}
      open={isMenuOpen}
      onClose={handleMenuClose}
    >
      <MenuItem onClick={handleMenuClose}>Profile</MenuItem>
      <MenuItem onClick={handleMenuClose}>My account</MenuItem>
      <MenuItem onClick={logOut}>Se déconnecter</MenuItem>
    </Menu>
  );

  const mobileMenuId = 'primary-search-account-menu-mobile';


  return (
    <div className={classes.grow}>
      <AppBar position="static">
        <Toolbar>
          <ButtonBase>
            <ArrowBackIcon
              edge="start"
              className={classes.menuButton}
              color="inherit"
              aria-label="previous"
              onClick={event => toPrevious(event, "/")}
            >
            </ArrowBackIcon>
          </ButtonBase>
          <ButtonBase>
            <Typography onClick={event => toHome(event, "/")} className={classes.title} variant="h6" noWrap>
              Ticketing
            </Typography>
          </ButtonBase>
          <div className={classes.search}>
            <div className={classes.searchIcon}>
              <SearchIcon />
            </div>
            <InputBase
              placeholder="Search…"
              classes={{
                root: classes.inputRoot,
                input: classes.inputInput,
              }}
              inputProps={{ 'aria-label': 'search' }}
            />
          </div>
          <div className={classes.grow} />
          <div className={classes.sectionDesktop}>

            {
              authenticated ? (
                <>
                  <IconButton aria-label="show 4 new mails" color="inherit" onClick={() => navigateTo("/tickets")}>

                    {/* <Badge badgeContent={8} color="secondary"> */}
                    <MonetizationOnIcon />
                    <Typography>{ammount}  </Typography>


                    {/* </Badge> */}
                  </IconButton>

                  <IconButton aria-label="show 4 new mails" color="inherit" onClick={() => navigateTo("/tickets")}>

                    <Badge badgeContent={8} color="secondary">
                      <Typography>Mes tikets    </Typography>

                    </Badge>
                  </IconButton>
                  <IconButton aria-label="show 17 new notifications" color="inherit">
                    <Badge badgeContent={17} color="secondary">
                      <NotificationsIcon />
                    </Badge>
                  </IconButton>

                  <IconButton
                    //edge="end"
                    aria-label="login"
                    aria-controls={menuId}
                    aria-haspopup="true"
                    onClick={handleProfileMenuOpen}
                    color="inherit"
                  >

                    <AccountCircle />
                  </IconButton>
                </>) : (
                  <IconButton
                    edge="end"
                    aria-label="se connecter"
                    aria-controls={menuId}
                    aria-haspopup="true"
                    onClick={openLoginButton}
                    color="inherit"
                  >

                    <ExitToAppOutlinedIcon />
                  </IconButton>
                )
            }


          </div>
          <div className={classes.sectionMobile}>
            <IconButton
              aria-label="show more"
              aria-controls={mobileMenuId}
              aria-haspopup="true"
              onClick={handleMobileMenuOpen}
              color="inherit"
            >
              <MoreIcon />
            </IconButton>
          </div>
        </Toolbar>
      </AppBar>
      <LoginForm open={open} close={handleClose} />
      {renderMenu}
    </div>
  );
}
