import React from 'react';
import Button from '@material-ui/core/Button';
import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';
import { makeStyles } from '@material-ui/core/styles';

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
    '& > * + *': {
      marginTop: theme.spacing(2),
    },
  },
}));

export default function SnackBar(props) {
  const classes = useStyles();
  var { message, color, close, icon, place, open } = props;
  console.log(open);

  const handleClose = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }
    open = false;
  };
  return (
    <div className={classes.root}>
      <Snackbar
        open={open}
        autoHideDuration={6000}
        onClose={() => props.closeNotification()}>       
        <Alert onClose={handleClose} severity={color}>
          {message}
        </Alert>
      </Snackbar>  
    </div>
  );
}