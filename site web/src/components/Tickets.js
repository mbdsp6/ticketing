import React, { Component } from "react";
import EvenementService from "../services/EvenementService";
import Utils from "../utils/Utils";
import { Link } from "react-router-dom";
import { makeStyles, withStyles } from '@material-ui/core/styles';
import Table from '../components/Table/Table';
import Card from "../components/Card/Card";
import CardHeader from "../components/Card/CardHeader";
import CardBody from "../components/Card/CardBody";
import PropTypes from 'prop-types';
import Paper from '@material-ui/core/Paper';
import TicketService from '../services/TicketService';


const useStyles = theme => ({
    cardCategoryWhite: {
        "&,& a,& a:hover,& a:focus": {
            color: "rgba(255,255,255,.62)",
            margin: "0",
            fontSize: "14px",
            marginTop: "0",
            marginBottom: "0"
        },
        "& a,& a:hover,& a:focus": {
            color: "#FFFFFF"
        }
    },
    cardTitleWhite: {
        color: "#FFFFFF",
        marginTop: "0px",
        minHeight: "auto",
        fontWeight: "300",
        fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
        marginBottom: "3px",
        textDecoration: "none",
        "& small": {
            color: "#777",
            fontSize: "65%",
            fontWeight: "400",
            lineHeight: "1"
        }
    },
    root: {
        display: 'flex',
        justifyContent: 'center',
        marginTop: 20

    }

});

class Tickets extends Component {
    constructor(props) {
        super(props);
        this.state = {
            tickets: []
        }

    }
    loadTickets() {
        TicketService.getMyTickets(1).then((response) => {
            this.setState({
                tickets: response.data
            });
        }).catch((error) => {
            console.log(error);
        });
    }
    componentDidMount() {
        this.loadTickets();
    }
    render() {
        Tickets.propTypes = {
            classes: PropTypes.object.isRequired,
        };
        const { classes } = this.props;
        return (
            <div className={classes.root}>
                <Card style={{ width: 1000 }}>
                    <CardHeader color="primary">
                        <h4 className={classes.cardTitleWhite}>Mes tickets disponibles</h4>
                        <p className={classes.cardCategoryWhite}>
                            Here is a subtitle for this table
                            </p>
                    </CardHeader>
                    <CardBody>
                        <Table
                            style={{ height: 200 }}
                            tableHeaderColor="primary"
                            tableHead={["N°", "Evenement", "Lieu", "Date début", "Type", ""]}
                            tableData={this.state.tickets}
                        />
                    </CardBody>
                </Card>
            </div>


        );
    }
}
export default withStyles(useStyles)(Tickets);