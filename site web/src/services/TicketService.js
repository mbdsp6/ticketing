import http from "../http-common";

class TicketService {
  acheterTicket(data){
    return http.post("/tickets/acheterticket", data);
  } 
  getMyTickets(id){
    return http.get(`/tickets/${id}`);
  }
}

export default new TicketService();