import http from "../http-common";

class UtilisateurService {
  login(username,password){
    var url = "/utilisateur/seconnecter?username="+username+"&password="+password;
    return http.post(url);
  } 
 
  checkUserStorage(){

  }

  logOut() {
    localStorage.removeItem("userStorage");   
  }

  getUserAmount(id){
    return http.get(`/utilisateur/amount/${id}`);
  }
}

export default new UtilisateurService();