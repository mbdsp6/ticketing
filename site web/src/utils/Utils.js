import {Date} from "prismic-reactjs";

class Utils {
    renderDateFormat(date){
        return new Intl.DateTimeFormat('en-US', 
        {year: 'numeric', 
        month: '2-digit',
        day: '2-digit'}).format(Date(date));
    }    
    renderTime(date){
        return Intl.DateTimeFormat('en-US',{
          hour: "numeric",
          minute: "numeric"}).format(Date(date));
      }
}

export default new Utils();