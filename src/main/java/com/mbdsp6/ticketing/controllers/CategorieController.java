package com.mbdsp6.ticketing.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import com.mbdsp6.ticketing.model.Categorie;
import com.mbdsp6.ticketing.service.ICategorieService;
import com.mbdsp6.ticketing.service.IEvenementService;


@RestController
@RequestMapping("api/ticketing/categorie")
public class CategorieController {
	@Autowired
    private ICategorieService categorieService;
	
	@Autowired
    private IEvenementService evenementService;
	
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public ResponseEntity<Object> create(@RequestBody Categorie categorie){
		try {
			categorieService.save(categorie);
			return new ResponseEntity<>(categorie,HttpStatus.CREATED);
		}catch(Exception e) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage(), e);
		}
	}
	
	@RequestMapping(value = "/creates", method = RequestMethod.POST)
	public ResponseEntity<Object> create(@RequestParam(name="evenement",required=true) Long evenement,
			@RequestParam(name="libelle",required=true) String libelle,
			@RequestParam(name="prix",required=true) double prix,
			@RequestParam(name="nombrebillet",required=true) int nombrebillet){
		try {
			Categorie categorie = new Categorie();
			categorie.setLibelle(libelle);
			categorie.setNombrebillet(nombrebillet);
			categorie.setPrix(prix);
			categorie.setEvenement(evenementService.findById(evenement));
			categorieService.save(categorie);
			return new ResponseEntity<>(categorie,HttpStatus.CREATED);
		}catch(Exception e) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage(), e);
		}
	}
	
	
	
}
