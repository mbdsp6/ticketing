package com.mbdsp6.ticketing.controllers;

import java.awt.PageAttributes.MediaType;
import java.sql.SQLDataException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.web.servlet.MultipartAutoConfiguration;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mbdsp6.ticketing.model.Categorie;
import com.mbdsp6.ticketing.model.Etat;
import com.mbdsp6.ticketing.model.Evenement;
import com.mbdsp6.ticketing.service.IEtatService;
import com.mbdsp6.ticketing.service.IEvenementService;
import com.mbdsp6.ticketing.service.IFileStorageService;
import com.mbdsp6.ticketing.utilitaire.Utilitaire;

@RestController
@RequestMapping("api/ticketing/evenement")
public class EvenementController {

	@Autowired
	private IEvenementService evenementService;
	
	@Autowired
	private IEtatService etatService;

	@Autowired
	private IFileStorageService fileService;

	private static ObjectMapper om = new ObjectMapper();

	@RequestMapping(value = "/create", consumes = { "multipart/form-data" }, method = RequestMethod.POST)
	public ResponseEntity<Object> createEvent(@RequestPart(value = "evenement") String event,
			@RequestPart(value = "image") MultipartFile file) throws Exception {
		Evenement evenement = om.readValue(event, Evenement.class);
		evenement.setImagebyte(Utilitaire.compressBytes(file.getBytes()));	
		evenement.setImage(file.getOriginalFilename());
		//fileService.save(file).toString();
		if (evenement.getNombreTicketRestant() >= 0) {
			for (Categorie iterable_element : evenement.getCategories()) {
				iterable_element.setEvenement(evenement);
			}
			evenementService.save(evenement);		
		}
		return new ResponseEntity<>(evenement, HttpStatus.OK);
	}

	@RequestMapping(value = "/events", method = RequestMethod.GET)
	public ResponseEntity<Object> events() {
		return new ResponseEntity<>(evenementService.findAll(), HttpStatus.OK);
	}

	@RequestMapping(value = "/event/{id}", method = RequestMethod.GET)
	public ResponseEntity<Object> eventById(@PathVariable("id") Long id) throws JsonProcessingException {
		return new ResponseEntity<>(om.writeValueAsString(evenementService.statistiqueTicketVendu(id)), HttpStatus.OK);
	}

	@RequestMapping(value = "/eventuser/{iduser}")
	public ResponseEntity<Object> eventByuser(@PathVariable("iduser") Long iduser) throws JsonProcessingException {
		return new ResponseEntity<Object>(evenementService.findByUtilisateurIduser(iduser), HttpStatus.OK);
	}
	
	@RequestMapping(value = "/eventstate/{id}/{etat}")
	public ResponseEntity<Object> changeEventState(@PathVariable("id") Long id,@PathVariable("etat") Long etat) throws JsonProcessingException, SQLDataException {
		Evenement event = evenementService.findById(id);
		Etat etatObject = etatService.findById(etat);
		event.setEtat(etatObject);
		evenementService.save(event);
		return new ResponseEntity<Object>(event, HttpStatus.OK);
	}

}
