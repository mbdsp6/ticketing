package com.mbdsp6.ticketing.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mbdsp6.ticketing.service.IFileStorageService;

@RestController
@RequestMapping("api/images")
public class FileStorageController {
	@Autowired
	private IFileStorageService fileService;
	
	@GetMapping("/{filename:.+}")
	public ResponseEntity<Resource> loadImage(@PathVariable String filename) {
		Resource file = fileService.load(filename);
	    return ResponseEntity.ok()
	        .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + file.getFilename() + "\"").body(file);
	}
}
