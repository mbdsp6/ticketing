package com.mbdsp6.ticketing.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.mbdsp6.ticketing.service.IRoleService;

@RestController
@RequestMapping("/")
public class RoleController {
	@Autowired
    private IRoleService roleService;
	
	@GetMapping("/roles")
	public ResponseEntity<Object>  findRoles(Model model) {
		return new ResponseEntity<>(roleService.findAll(),HttpStatus.OK);
	}
}
