package com.mbdsp6.ticketing.controllers;

import java.sql.SQLDataException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mbdsp6.ticketing.model.Categorie;
import com.mbdsp6.ticketing.model.CommandeTicket;
import com.mbdsp6.ticketing.model.Evenement;
import com.mbdsp6.ticketing.model.Ticket;
import com.mbdsp6.ticketing.model.Utilisateur;
import com.mbdsp6.ticketing.model.Etat;
import com.mbdsp6.ticketing.service.ICategorieService;
import com.mbdsp6.ticketing.service.IEtatService;
import com.mbdsp6.ticketing.service.IEvenementService;
import com.mbdsp6.ticketing.service.ITicketService;
import com.mbdsp6.ticketing.service.IUtilisateurService;
import com.mbdsp6.ticketing.utilitaire.Utilitaire;

@RestController
@RequestMapping("/api/ticketing")
public class TicketController {
	@Autowired
	private ITicketService ticketService;
	
	@Autowired
	private IUtilisateurService utilisateurService;

	@Autowired
	private IEtatService etatService;
	
	private static ObjectMapper om = new ObjectMapper();

	@GetMapping("/tickets/{iduser}")
	public ResponseEntity<Object> getTicketsByUser(@PathVariable("iduser") Long iduser) throws Exception {
		Utilisateur user = new Utilisateur();
		user.setIduser(iduser);
		return new ResponseEntity<>(ticketService.findByUtilisateur(user), HttpStatus.OK);
	}

	@PostMapping("/tickets/buy")
	public ResponseEntity<Object> save(@RequestBody List<Ticket> tickets) {
		ticketService.save(tickets);
		return new ResponseEntity<>(tickets, HttpStatus.OK);
	}

	@PostMapping("/tickets/acheterticket")
	public ResponseEntity<Object> saveticket(@RequestBody CommandeTicket commande) throws Exception {

		List<Ticket> tickets = new ArrayList<Ticket>();
		// Nombre billet demander par categorie <= nb billet disponible par categorie

		double totalPrice = 0;
		for (Categorie categorie : commande.getCategories()) {
			if(ticketService.nombreTicketDisponibleParCategorie(commande.getIdevenement(),categorie.getIdcategorie())>=categorie.getNombrebillet()) {
				totalPrice+=categorie.getPrix()*categorie.getNombrebillet();
				for (int i = 0; i < categorie.getNombrebillet(); i++) {
					Ticket ticket = new Ticket();
					ticket.setCategorie(categorie);
					ticket.setEvenement(new Evenement(commande.getIdevenement()));
					ticket.setNumeroticket(Utilitaire.generateTicketNumber(commande.getIdevenement(), categorie.getIdcategorie()));
					ticket.setUtilisateur(new Utilisateur(commande.getIduser()));
					ticket.setEtat(new Etat(Long.decode("1")));
					tickets.add(ticket);
					
				}						
			} else {
				throw new ResponseStatusException(HttpStatus.EXPECTATION_FAILED,
						"Le nombre de ticket doit etre inferieur ou egal au nombre de ticket disponible");
			}			
		}
		// Verifier la somme jetons user >= somme prix billet
		utilisateurService.checkUserAmount(commande.getIduser(), totalPrice);
		ticketService.save(tickets);
		utilisateurService.debiterUserAmount(commande.getIduser(), totalPrice);
		return new ResponseEntity<>("Achat ticket avec succées",HttpStatus.OK);
	}
	
	@RequestMapping(value="/tickets/mytickets",method = RequestMethod.GET)
	public ResponseEntity<Object> getTicketsByEventUser(@RequestParam(name="iduser") Long iduser,
			@RequestParam("idevenement") Long idevenement) throws JsonProcessingException{
		return new ResponseEntity<Object>(ticketService.findByEvenementIdevenementAndUtilisateurIduser(idevenement, iduser),HttpStatus.OK);
	}
	
	@RequestMapping(value = "/ticketstate/{id}/{etat}")
	public ResponseEntity<Object> changeTicketState(@PathVariable("id") Long id,@PathVariable("etat") Long etat) throws Exception {
	
		Ticket ticket  = ticketService.findById(id);
		Etat etatObject = etatService.findById(etat);
		ticket.setEtat(etatObject);		
		ticketService.save(ticket);
		return new ResponseEntity<Object>(ticket, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/ticketpage")
	@ResponseBody
	public String checkingState() {
		return "home";
	}
}
