package com.mbdsp6.ticketing.controllers;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.mbdsp6.ticketing.model.Etat;
import com.mbdsp6.ticketing.model.Ticket;
import com.mbdsp6.ticketing.service.IEtatService;
import com.mbdsp6.ticketing.service.ITicketService;
import com.mbdsp6.ticketing.service.IUtilisateurService;
import com.mbdsp6.ticketing.utilitaire.Utilitaire;


@Controller
@RequestMapping("api/ticketing")
public class TicketingController  {
	@Autowired
	private ITicketService ticketService;
	
	@Autowired
	private IUtilisateurService utilisateurService;

	@Autowired
	private IEtatService etatService;
	
	@GetMapping("/checkTicket/{id}")
	public ModelAndView  get(@PathVariable("id") Long id){
		ModelAndView mav = new ModelAndView();    
	    mav.setViewName("home");
		Ticket ticket;
		try {
			ticket = ticketService.findById(id);
			if(!Utilitaire.checkEtatCloturer(ticket)) {
				//change etat to cloturer
				Etat etatObject = etatService.findById(4L);
				ticket.setEtat(etatObject);
				ticketService.save(ticket);
				mav.addObject("message", "Ticket Valide!");
			}else {
				mav.addObject("message", "Ticket déjà utiliser!");
			}			
			
		} catch (Exception e) {			
			e.printStackTrace();
		}
							
		return mav;
	}
	
	
}
