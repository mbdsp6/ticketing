package com.mbdsp6.ticketing.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;
import com.mbdsp6.ticketing.model.Utilisateur;
import com.mbdsp6.ticketing.service.IEtatService;
import com.mbdsp6.ticketing.service.IRoleService;
import com.mbdsp6.ticketing.service.IUtilisateurService;

@RestController
@RequestMapping("api/ticketing/utilisateur")
public class UtilisateurController {
	@Autowired
	private IUtilisateurService utilisateurService;

	@Autowired
	private IRoleService roleService;

	@Autowired
	private IEtatService etatService;

	@RequestMapping(value = "/inscription", method = RequestMethod.POST)
	public ResponseEntity<Object> inscription(@RequestParam(name = "etat", required = true) Long etat,
			@RequestParam(name = "role", required = true) Long role,
			@RequestParam(name = "nom", required = true) String nom,
			@RequestParam(name = "username", required = true) String username,
			@RequestParam(name = "password", required = true) String password,
			@RequestParam(name = "confirmpassword", required = true) String confirmpassword) throws Exception {
		Utilisateur user = new Utilisateur();
		user.setRole(roleService.findById(role));
		user.setEtat(etatService.findById(etat));
		user.setUsername(username);
		user.setPassword(password);
		user.setNom(nom);
		utilisateurService.save(user);
		user.setPassword("");
		return new ResponseEntity<Object>(user, HttpStatus.OK);
	}

	@RequestMapping(value = "/updateuser", method = RequestMethod.POST)
	public ResponseEntity<Object> update(@RequestBody Utilisateur utilisateur) {
		utilisateurService.save(utilisateur);
		return new ResponseEntity<Object>(utilisateur, HttpStatus.OK);
	}

	@RequestMapping(value = "/seconnecter", method = RequestMethod.POST)
	public ResponseEntity<Object> login(@RequestParam(name = "username") String username,
			@RequestParam(name = "password") String password) {

		Utilisateur user = utilisateurService.findByUsernameAndPassword(username, password);
		if (user == null) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Verifier username et mot de passe");
		}
		return new ResponseEntity<Object>(user, HttpStatus.OK);

	}
	
	@RequestMapping(value = "/amount/{id}", method = RequestMethod.GET)
	public ResponseEntity<Object> useramount(@PathVariable("id") Long id) throws Exception {

		double amount = utilisateurService.userAmount(id);
		
		return new ResponseEntity<Object>(amount, HttpStatus.OK);

	}
}
