package com.mbdsp6.ticketing.model;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
@Table(name="categorie")
public class Categorie {
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator = "categorie_Sequence")
    @SequenceGenerator(name = "categorie_Sequence", sequenceName = "CATEGORIE_SEQ")
    private Long idcategorie;
	
	private String libelle;
	private double prix;
	private int nombrebillet;
	@Transient
	private int nombreticketdisponible;
	
	@Transient
	private int nombreticketventdu;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "idevenement", referencedColumnName = "idevenement")
	@JsonBackReference
	private Evenement evenement;
	
	public Categorie() {
		super();
	}
	
	
	public String getLibelle() {
		return libelle;
	}
	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}
	public double getPrix() {
		return prix;
	}
	public void setPrix(double prix) {
		this.prix = prix;
	}
	public int getNombrebillet() {
		return nombrebillet;
	}
	public void setNombrebillet(int nombrebillet) {
		this.nombrebillet = nombrebillet;
	}


	public Long getIdcategorie() {
		return idcategorie;
	}


	public void setIdcategorie(Long idcategorie) {
		this.idcategorie = idcategorie;
	}


	public Evenement getEvenement() {
		return evenement;
	}


	public void setEvenement(Evenement evenement) {
		this.evenement = evenement;
	}


	public int getNombreticketdisponible() {
		return nombreticketdisponible;
	}


	public void setNombreticketdisponible(int nombreticketdisponible) {
		this.nombreticketdisponible = nombreticketdisponible;
	}


	public int getNombreticketventdu() {
		return nombreticketventdu;
	}


	public void setNombreticketventdu(int nombreticketventdu) {
		this.nombreticketventdu = nombreticketventdu;
	}

	




	
	
}
