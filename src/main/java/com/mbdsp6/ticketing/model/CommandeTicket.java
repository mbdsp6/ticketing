package com.mbdsp6.ticketing.model;

import java.util.List;

public class CommandeTicket {
	private Long iduser;
	private Long idevenement;
	private List<Categorie> categories;
	
	
	public CommandeTicket() {
		super();
	}
	
	
	public Long getIduser() {
		return iduser;
	}
	public void setIduser(Long iduser) {
		this.iduser = iduser;
	}
	public Long getIdevenement() {
		return idevenement;
	}
	public void setIdevenement(Long idevenement) {
		this.idevenement = idevenement;
	}
	public List<Categorie> getCategories() {
		return categories;
	}
	public void setCategories(List<Categorie> categories) {
		this.categories = categories;
	}
	
	
}
