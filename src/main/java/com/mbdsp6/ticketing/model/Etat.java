package com.mbdsp6.ticketing.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="etat")
public class Etat {

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator = "etat_Sequence")
    @SequenceGenerator(name = "etat_Sequence", sequenceName = "ETAT_SEQ")
    private Long idetat;
	
	private String libelle;

	
	
	public Etat() {
		super();
	}
	
	public Etat(Long idetat) {
		super();
		this.idetat = idetat;
	}

	public Etat(Long idetat, String libelle) {
		super();
		this.idetat = idetat;
		this.libelle = libelle;
	}




	public Long getIdetat() {
		return idetat;
	}

	public void setIdetat(Long idetat) {
		this.idetat = idetat;
	}

	public String getLibelle() {
		return libelle;
	}

	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}
	
	
}
