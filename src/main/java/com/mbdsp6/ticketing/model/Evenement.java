package com.mbdsp6.ticketing.model;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.mbdsp6.ticketing.utilitaire.Utilitaire;

import ch.qos.logback.classic.pattern.Util;

/**
 * @author BICI
 *
 */
@Entity
@Table(name="evenement")
public class Evenement {
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator = "evenement_Sequence")
    @SequenceGenerator(name = "evenement_Sequence", sequenceName = "EVENEMENT_SEQ", allocationSize = 1)
    private Long idevenement;
		
	private String libelle;
	
	private String description;
	
	private Date datedebut;
	
	private Date datefin;
	
	private int nombrebillet;
	
	private String lieu;
	
	private String image;
	
	private byte[] imagebyte;
	
	
	@OneToMany(fetch = FetchType.LAZY,mappedBy = "evenement", cascade=CascadeType.ALL)
	@JsonManagedReference
	private List<Categorie> categories;
	
	@ManyToOne
	@JoinColumn(name = "idetat", referencedColumnName = "idetat")
	private Etat etat;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JsonBackReference
	@JoinColumn(name = "iduser", referencedColumnName = "iduser")
	private Utilisateur utilisateur;
	
	@Transient
	private int nombreTicketRestant;
	
	@Transient
	private int nombreTicketVendu;
	

	public Evenement() {
		super();
	}

	
	public Evenement(Long idevenement) {
		super();
		this.idevenement = idevenement;
	}

	public Long getIdevenement() {
		return idevenement;
	}

	public void setIdevenement(Long idevenement) {
		this.idevenement = idevenement;
	}

	public String getLibelle() {
		return libelle;
	}

	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getDatedebut() {
		return datedebut;
	}

	public void setDatedebut(Date datedebut) {
		this.datedebut = datedebut;
	}

	public Date getDatefin() {
		return datefin;
	}

	public void setDatefin(Date datefin) {
		this.datefin = datefin;
	}

	public int getNombrebillet() {
		return nombrebillet;
	}

	public void setNombrebillet(int nombrebillet) {
		this.nombrebillet = nombrebillet;
	}

	public String getLieu() {
		return lieu;
	}

	public void setLieu(String lieu) {
		this.lieu = lieu;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	
	
	public List<Categorie> getCategories() {
		return categories;
	}

	public void setCategories(List<Categorie> categories) {
		this.categories = categories;
	}
	
	public void setCategorie(Categorie categorie) {
		this.categories.add(categorie);
		categorie.setEvenement(this);
	}

	public Etat getEtat() {
		return etat;
	}

	public void setEtat(Etat etat) {
		this.etat = etat;
	}

	public Utilisateur getUtilisateur() {
		return utilisateur;
	}

	public void setUtilisateur(Utilisateur utilisateur) {
		this.utilisateur = utilisateur;
	}

	public int getNombreTicketRestant() throws Exception {
		int rep = 0;
		for (Categorie categorie : categories) {
			rep += categorie.getNombrebillet();
		}
		if(this.nombrebillet<rep) {
			throw new Exception("La somme des nombres de billet par categorie doit etre inferieur au total des tickets ");
		}
		return nombreTicketRestant;
	}


	public int getNombreTicketVendu() {
		return nombreTicketVendu;
	}

	public void setNombreTicketVendu(int nombreTicketVendu) {
		this.nombreTicketVendu = nombreTicketVendu;
	}


	public void setNombreTicketRestant(int nombreTicketRestant) {
		this.nombreTicketRestant = nombreTicketRestant;
	}


	public byte[] getImagebyte() {
		return Utilitaire.decompressBytes(imagebyte) ;
	}


	public void setImagebyte(byte[] imagebyte) {
		this.imagebyte = imagebyte;
	}
	
	
	

	
	
	
	
	
	
	
}
