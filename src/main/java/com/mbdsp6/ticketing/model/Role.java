package com.mbdsp6.ticketing.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="role")
public class Role {
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator = "role_Sequence")
    @SequenceGenerator(name = "role_Sequence", sequenceName = "ROLE_SEQ")
    private Long idrole;
	
	private String libelle;
	
	

	public Role() {
	}
	
	

	public Role(Long idrole, String libelle) {
		super();
		this.idrole = idrole;
		this.libelle = libelle;
	}



	public String getLibelle() {
		return libelle;
	}

	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}
	
	
}
