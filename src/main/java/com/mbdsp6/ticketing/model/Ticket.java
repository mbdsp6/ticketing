package com.mbdsp6.ticketing.model;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name="ticket")
public class Ticket {
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator = "ticket_Sequence")
    @SequenceGenerator(name = "ticket_Sequence", sequenceName = "TICKET_SEQ")
    private Long idticket;
	
	private String numeroticket;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "idevenement", referencedColumnName = "idevenement")
	private Evenement evenement;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JsonManagedReference
	@JoinColumn(name = "idetat", referencedColumnName = "idetat")
	private Etat etat;
	
	@ManyToOne
	@JoinColumn(name = "idcategorie", referencedColumnName = "idcategorie")
	private Categorie categorie;
	
	@ManyToOne(fetch = FetchType.LAZY, optional=false)
	@JsonBackReference
	@JoinColumn(name = "iduser", referencedColumnName = "iduser")
	private Utilisateur utilisateur;

	public Ticket() {
		super();
	}

	public Ticket(Long idticket, String numeroticket, Evenement evenement, Etat etat, Categorie categorie,
			Utilisateur utilisateur) {
		super();
		this.idticket = idticket;
		this.numeroticket = numeroticket;
		this.evenement = evenement;
		this.etat = etat;
		this.categorie = categorie;
		this.utilisateur = utilisateur;
	}

	public Long getIdticket() {
		return idticket;
	}

	public void setIdticket(Long idticket) {
		this.idticket = idticket;
	}

	public String getNumeroticket() {
		return numeroticket;
	}

	public void setNumeroticket(String numeroticket) {
		this.numeroticket = numeroticket;
	}

	public Evenement getEvenement() {
		return evenement;
	}

	public void setEvenement(Evenement evenement) {
		this.evenement = evenement;
	}

	public Etat getEtat() {
		return etat;
	}

	public void setEtat(Etat etat) {
		this.etat = etat;
	}

	public Categorie getCategorie() {
		return categorie;
	}

	public void setCategorie(Categorie categorie) {
		this.categorie = categorie;
	}

	public Utilisateur getUtilisateur() {
		return utilisateur;
	}

	public void setUtilisateur(Utilisateur utilisateur) {
		this.utilisateur = utilisateur;
	}
	
	
	
	
}
