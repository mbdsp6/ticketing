package com.mbdsp6.ticketing.model;

public class UserAmountMap {

	private String __v;
	private String id;
	private long user;
	private double amount;
	
	
	public String getId() {
		return id;
	}


	public void setId(String id) {
		this.id = id;
	}


	public long getUser() {
		return user;
	}


	public void setUser(long user) {
		this.user = user;
	}


	public double getAmount() {
		return amount;
	}


	public void setAmount(double amount) {
		this.amount = amount;
	}


	

	public String get__v() {
		return __v;
	}


	public void set__v(String __v) {
		this.__v = __v;
	}


	public UserAmountMap() {
		super();
	}
	
	
	
	
}
