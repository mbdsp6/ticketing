package com.mbdsp6.ticketing.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;


@Entity
@Table(name="utilisateur")
public class Utilisateur {
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator = "utilisateur_Sequence")
    @SequenceGenerator(name = "utilisateur_Sequence", sequenceName = "TICKET_SEQ")
    private Long iduser;
	
	@javax.validation.constraints.NotBlank(message = "Le champ nom est obligatoire")
	private String nom;
	
	@javax.validation.constraints.NotBlank(message = "Le champ identifiant est obligatoire")
	private String username;
	
	@javax.validation.constraints.NotBlank(message = "Le champ mot de passe est obligatoire")
	@JsonIgnore
	private String password;
	
	@ManyToOne
	@JoinColumn(name = "idrole", referencedColumnName = "idrole")
	private Role role;
	
	@ManyToOne
	@JoinColumn(name = "idetat", referencedColumnName = "idetat")
	private Etat etat;

	public Utilisateur() {
		super();
	}
	
	public Utilisateur(Long iduser) {
		super();
		this.iduser = iduser;
	}
	
	public Utilisateur(Long iduser, String nom, String username, String password, Role role, Etat etat) {
		super();
		this.iduser = iduser;
		this.nom = nom;
		this.username = username;
		this.password = password;
		this.role = role;
		this.etat = etat;
	}

	public Long getIduser() {
		return iduser;
	}

	public void setIduser(Long iduser) {
		this.iduser = iduser;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public Etat getEtat() {
		return etat;
	}

	public void setEtat(Etat etat) {
		this.etat = etat;
	}
	
	
	
}
