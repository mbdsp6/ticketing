package com.mbdsp6.ticketing.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.mbdsp6.ticketing.model.Categorie;
import com.mbdsp6.ticketing.model.Evenement;

@Repository
public interface CategorieRepository extends CrudRepository<Categorie,Long> {
	List<Categorie> findByEvenement(Evenement evenement);
	List<Categorie> findByEvenementIdevenement(Long idevenement);
}
