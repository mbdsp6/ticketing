package com.mbdsp6.ticketing.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.mbdsp6.ticketing.model.Etat;

@Repository 
public interface EtatRepository extends CrudRepository<Etat,Long>{
	
}
