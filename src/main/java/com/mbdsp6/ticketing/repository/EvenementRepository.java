package com.mbdsp6.ticketing.repository;


import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.mbdsp6.ticketing.model.Evenement;

@Repository
public interface EvenementRepository extends CrudRepository<Evenement,Long>{
	List<Evenement> findByUtilisateurIduser(Long iduser);
}
