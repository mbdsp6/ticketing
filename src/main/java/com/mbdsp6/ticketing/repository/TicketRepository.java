package com.mbdsp6.ticketing.repository;

import java.util.List;
import java.util.Optional;


import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.mbdsp6.ticketing.model.Categorie;
import com.mbdsp6.ticketing.model.Evenement;
import com.mbdsp6.ticketing.model.Ticket;
import com.mbdsp6.ticketing.model.Utilisateur;

public interface TicketRepository extends CrudRepository<Ticket,Long> {
	List<Ticket> findByUtilisateur(Utilisateur utilisateur);
	List<Ticket> findByEvenementIdevenement(Long idevenement);
	List<Ticket> findByEvenement(Evenement evenement);
	
	@Query("SELECT t FROM Ticket t WHERE t.evenement=:evenement and "
			+ "t.categorie IN :categories")
	List<Ticket> findByEvenementAndCategorie(@Param("evenement")Evenement evenement, @Param("categories") List<Categorie> categories);
	

	@Query("SELECT c.nombrebillet-COUNT(t.categorie.idcategorie) FROM Ticket t RIGHT JOIN t.categorie c "
			+ "WHERE c.evenement.idevenement=:event and "
			+ "c.idcategorie=:categorie GROUP BY c, c.nombrebillet")
	Optional<Integer> nombreTicketParCategorie(@Param("event") Long evenement,@Param("categorie") Long categorie);
	
	List<Ticket> findByEvenementIdevenementAndUtilisateurIduser(Long idevenement, Long iduser);

	
}
