package com.mbdsp6.ticketing.repository;

import java.util.List;


import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.mbdsp6.ticketing.model.Utilisateur;

@Repository
public interface UtilisateurRepository extends CrudRepository<Utilisateur,Long> {
	public List<Utilisateur> findByUsernameAndPassword(@Param("username") String username, @Param("password") String password);
} 
