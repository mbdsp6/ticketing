package com.mbdsp6.ticketing.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mbdsp6.ticketing.model.Categorie;
import com.mbdsp6.ticketing.repository.CategorieRepository;

@Service
public class CategorieService implements ICategorieService{

	@Autowired
	private CategorieRepository repository;
	
	@Override
	public Optional<Categorie> findById(Long id) {
		return repository.findById(id);
	}

	@Override
	public List<Categorie> findAll() {
		return (List<Categorie>) repository.findAll();
	}

	@Override
	public void save(Categorie categorie) {
		repository.save(categorie);
	}

	@Override
	public List<Categorie> findByEvenementIdevenement(Long idevenement) {
		// TODO Auto-generated method stub
		return repository.findByEvenementIdevenement(idevenement);
	}
	
}
