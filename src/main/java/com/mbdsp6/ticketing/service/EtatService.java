package com.mbdsp6.ticketing.service;

import java.sql.SQLDataException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mbdsp6.ticketing.model.Etat;
import com.mbdsp6.ticketing.repository.EtatRepository;

@Service
public class EtatService implements IEtatService {

	@Autowired
	private EtatRepository repository;
	
	@Override
	public List<Etat> findAll() {
		// TODO Auto-generated method stub
		return (List<Etat>) repository.findAll();
	}

	@Override
	public Etat findById(Long id) throws SQLDataException {
		return repository.findById(id).orElseThrow(()->new SQLDataException("Etat introuvable"));
	}

}
