package com.mbdsp6.ticketing.service;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.mbdsp6.ticketing.model.Categorie;
import com.mbdsp6.ticketing.model.Evenement;
import com.mbdsp6.ticketing.model.Ticket;
import com.mbdsp6.ticketing.repository.EvenementRepository;
import com.mbdsp6.ticketing.repository.TicketRepository;
import com.mbdsp6.ticketing.utilitaire.Utilitaire;

@Service
public class EvenementService implements IEvenementService{

	@Autowired
	private EvenementRepository repository;
	
	@Autowired
	private TicketRepository ticketRepository;

	
	@Override	
	@Transactional
	public List<Evenement> findAll() {
		// TODO Auto-generated method stub
		return (List<Evenement>) repository.findAll();
	}

	@Override
	public Evenement findById(Long id) {
		// TODO Auto-generated method stub
		return repository.findById(id).get();
	}

	@Override
	public void save(Evenement event) {
		repository.save(event);		
	}

	@Override
	public Evenement statistiqueTicketVendu(Long idevenement) {
		Evenement event = repository.findById(idevenement).get();
		List<Ticket> tickets = ticketRepository.findByEvenementAndCategorie(event,event.getCategories());	
		int nombreticketventdu=0;	
		int nombreticketTotal = 0;
		for (Categorie categorie : event.getCategories()) {
			nombreticketventdu = Utilitaire.nombreTicketVenduParCategorie(tickets,categorie);
			categorie.setNombreticketventdu(nombreticketventdu);
			categorie.setNombreticketdisponible(categorie.getNombrebillet()-nombreticketventdu);
			event.setNombreTicketVendu(nombreticketventdu+event.getNombreTicketVendu());	
			nombreticketTotal+=categorie.getNombrebillet();
		}
		event.setNombreTicketRestant(nombreticketTotal-event.getNombreTicketVendu());
		return event;
	}

	@Override
	public List<Evenement> findByUtilisateurIduser(Long iduser) {
		// TODO Auto-generated method stub
		return repository.findByUtilisateurIduser(iduser);
	}
	
	
	
}
