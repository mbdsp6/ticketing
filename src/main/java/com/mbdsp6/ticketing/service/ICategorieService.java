package com.mbdsp6.ticketing.service;

import java.util.List;
import java.util.Optional;

import com.mbdsp6.ticketing.model.Categorie;

public interface ICategorieService {
	
	public Optional<Categorie> findById(Long id);
	
	public List<Categorie> findAll();
	
	public void save(Categorie categorie);
	
	List<Categorie> findByEvenementIdevenement(Long idevenement);
	
	
}
