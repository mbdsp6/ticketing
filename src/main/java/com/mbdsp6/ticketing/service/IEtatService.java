package com.mbdsp6.ticketing.service;

import java.sql.SQLDataException;
import java.util.List;

import com.mbdsp6.ticketing.model.Etat;

public interface IEtatService {
	public List<Etat> findAll();
	public Etat findById(Long id) throws SQLDataException;
}
