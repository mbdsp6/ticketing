package com.mbdsp6.ticketing.service;

import java.util.List;

import com.mbdsp6.ticketing.model.Evenement;

public interface IEvenementService {
	List<Evenement> findAll();
	
	Evenement findById(Long id);
	
	void save(Evenement event);
	
	Evenement statistiqueTicketVendu(Long idevenement);
	
	List<Evenement> findByUtilisateurIduser(Long iduser);
	
	
}
