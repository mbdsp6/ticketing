package com.mbdsp6.ticketing.service;

import java.util.List;

import com.mbdsp6.ticketing.model.Role;

public interface IRoleService {
	public List<Role> findAll();
	
	public Role findById(Long id) throws Exception;
}
