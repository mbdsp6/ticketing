package com.mbdsp6.ticketing.service;

import java.util.List;


import com.mbdsp6.ticketing.model.Ticket;
import com.mbdsp6.ticketing.model.Utilisateur;

public interface ITicketService {

	List<Ticket> findAll();
	Ticket findById(Long id) throws Exception;
	void save(Ticket ticket);
	void save(List<Ticket> tickets);
	List<Ticket> findByUtilisateur(Utilisateur utilisateur);
	List<Ticket> findByEvenementIdevenement(Long idevenement);
	int nombreTicketDisponibleParCategorie(Long evenement,Long categorie);
	List<Ticket> findByEvenementIdevenementAndUtilisateurIduser(Long idevenement, Long iduser);
}
