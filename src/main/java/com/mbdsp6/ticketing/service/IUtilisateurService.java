package com.mbdsp6.ticketing.service;

import java.util.List;
import java.util.Optional;

import com.mbdsp6.ticketing.model.CommandeTicket;
import com.mbdsp6.ticketing.model.Utilisateur;

public interface IUtilisateurService {
	
	public Optional<Utilisateur> findById(Long id);
	public  List<Utilisateur> findAll();
	public void save(Utilisateur utilisateur);
	public Utilisateur findByUsernameAndPassword(String username, String password);
	
	public boolean checkUserAmount(long iduser,double totalAmount) throws Exception;
	
	public void debiterUserAmount(long iduser,double debit) throws Exception;
	
	public void crediterUserAmount(long iduser,double credit) throws Exception;
	
	public double userAmount(long iduser)throws Exception;
}
