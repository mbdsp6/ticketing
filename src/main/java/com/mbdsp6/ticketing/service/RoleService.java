package com.mbdsp6.ticketing.service;

import java.sql.SQLDataException;
import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mbdsp6.ticketing.model.Role;
import com.mbdsp6.ticketing.repository.RoleRepository;


@Service
public class RoleService implements IRoleService {

	@Autowired
	private RoleRepository repository;
	
	@Override
	public List<Role> findAll() {		
		return (List<Role>)repository.findAll();
	}

	@Override
	public Role findById(Long id) throws Exception {
		return repository.findById(id).orElseThrow(()->new SQLDataException("Role introuvable"));
	}

}
