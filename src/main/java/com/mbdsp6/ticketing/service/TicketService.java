package com.mbdsp6.ticketing.service;

import java.sql.SQLDataException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mbdsp6.ticketing.model.Ticket;
import com.mbdsp6.ticketing.model.Utilisateur;
import com.mbdsp6.ticketing.repository.TicketRepository;

@Service
public class TicketService implements ITicketService {

	@Autowired
	private TicketRepository repository;
	@Override
	public List<Ticket> findAll() {
		return (List<Ticket>) repository.findAll();
	}

	@Override
	public Ticket findById(Long id) throws Exception {
		return repository.findById(id).orElseThrow(()->new SQLDataException("Ticket introuvable"));
	}

	@Override
	public void save(Ticket ticket) {
		repository.save(ticket);
	}

	@Override
	public List<Ticket> findByUtilisateur(Utilisateur utilisateur) {
		// TODO Auto-generated method stub
		return repository.findByUtilisateur(utilisateur);
	}

	@Override
	public void save(List<Ticket> tickets) {
		repository.saveAll(tickets);		
	}

	@Override
	public List<Ticket> findByEvenementIdevenement(Long idevenement) {
		return repository.findByEvenementIdevenement(idevenement);
	}

	@Override
	public int nombreTicketDisponibleParCategorie(Long evenement,Long categorie) {
		return repository.nombreTicketParCategorie(evenement,categorie).orElse(null);
	}

	@Override
	@Transactional
	public List<Ticket> findByEvenementIdevenementAndUtilisateurIduser(Long idevenement, Long iduser) {
		// TODO Auto-generated method stub
		return repository.findByEvenementIdevenementAndUtilisateurIduser(idevenement, iduser);
	}
	
	
	
	

}
