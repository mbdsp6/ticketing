package com.mbdsp6.ticketing.service;


import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.mbdsp6.ticketing.model.CommandeTicket;
import com.mbdsp6.ticketing.model.UserAmountMap;
import com.mbdsp6.ticketing.model.Utilisateur;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mbdsp6.ticketing.model.Categorie;
import com.mbdsp6.ticketing.repository.UtilisateurRepository;


@Service
public class UtilisateurService implements IUtilisateurService {

	@Autowired
	private UtilisateurRepository repository;
	
	@Override
	public Optional<Utilisateur> findById(Long id) {
		return repository.findById(id);
	}

	@Override
	public List<Utilisateur> findAll() {
		return (List<Utilisateur>) repository.findAll();
	}

	@Override
	public void save(Utilisateur utilisateur) {
		repository.save(utilisateur);	
	}

	@Override
	public Utilisateur findByUsernameAndPassword(String username, String password) {
		return (repository.findByUsernameAndPassword(username, password).size() != 0)?repository.findByUsernameAndPassword(username, password).get(0):null;
	}

	@Override
	public boolean checkUserAmount(long iduser, double totalAmount) throws Exception {
		
		final String uri = "http://localhost:8484/api/useramount/"+iduser;
	    RestTemplate restTemplate = new RestTemplate();
	    UserAmountMap[] result = restTemplate.getForObject(uri, UserAmountMap[].class);
	    if(result[0].getAmount()>=totalAmount) {
	    	return true;
	    }
		throw new Exception("Montant insuffisant");
	}

	@Override
	public void debiterUserAmount(long iduser, double debit) throws Exception {
		final String uri = "http://localhost:8484/api/useramount/debiter/"+iduser+"/"+debit;
	    RestTemplate restTemplate = new RestTemplate();
	    try {
	    	String result = restTemplate.getForObject(uri, String.class);
	    }catch(Exception e) {
	    	throw e;
	    }	
	}

	@Override
	public void crediterUserAmount(long iduser, double credit) throws Exception {
		final String uri = "http://localhost:8484/api/useramount/crediter/"+iduser+"/"+credit;
	    RestTemplate restTemplate = new RestTemplate();
	    try {
	    	String result = restTemplate.getForObject(uri, String.class);
	    }catch(Exception e) {
	    	throw e;
	    }		
	}

	@Override
	public double userAmount(long iduser) throws Exception {
		final String uri = "http://localhost:8484/api/useramount/"+iduser;
	    RestTemplate restTemplate = new RestTemplate();
	    UserAmountMap[] result = restTemplate.getForObject(uri, UserAmountMap[].class);
	    return result[0].getAmount();    
	}

}
