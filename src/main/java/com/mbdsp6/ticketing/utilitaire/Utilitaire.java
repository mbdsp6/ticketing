package com.mbdsp6.ticketing.utilitaire;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.zip.DataFormatException;
import java.util.zip.Deflater;
import java.util.zip.Inflater;

import com.mbdsp6.ticketing.model.Categorie;
import com.mbdsp6.ticketing.model.Ticket;

public class Utilitaire {

	public static int nombreTicketVenduParCategorie(List<Ticket> tickets, Categorie categorie)
	{
		int nb=0;
		for (Ticket ticket : tickets) {
			if(ticket.getCategorie()==categorie) {
				nb++;
			}
		}
		return nb;
	}
	
	public static String generateTicketNumber(Long evenement, Long categorie) {
		return "EVT"+evenement+"_"+categorie;
	}
	
	public static byte[] compressBytes(byte[] data) {
		Deflater deflater = new Deflater();
		deflater.setInput(data);
		deflater.finish();
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream(data.length);
		byte[] buffer = new byte[1024];
		while (!deflater.finished()) {
			int count = deflater.deflate(buffer);
			outputStream.write(buffer, 0, count);
		}
		try {
			outputStream.close();
		} catch (IOException e) {
		}
		System.out.println("Compressed Image Byte Size - " + outputStream.toByteArray().length);
		return outputStream.toByteArray();
	}
	// uncompress the image bytes before returning it to the angular application
	public static byte[] decompressBytes(byte[] data) {
		Inflater inflater = new Inflater();
		inflater.setInput(data);
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream(data.length);
		byte[] buffer = new byte[1024];
		try {
			while (!inflater.finished()) {
				int count = inflater.inflate(buffer);
				outputStream.write(buffer, 0, count);
			}
			outputStream.close();
		} catch (IOException ioe) {
		} catch (DataFormatException e) {
		}
		return outputStream.toByteArray();
	}
	
	public static boolean checkEtatCloturer(Ticket ticket) {
		return ticket.getEtat().getIdetat()==4;
	}
	
	
	
}
