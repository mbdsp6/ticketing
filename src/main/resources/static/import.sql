/*==============================================================*/
/* Nom de SGBD :  ORACLE Version 11g                            */
/* Date de cr�ation :  30/07/2020 19:32:28                      */
/*==============================================================*/


alter table CATEGORIE
   drop constraint FK_CATEGORI_ASSOCIATI_EVENEMEN;

alter table EVENEMENT
   drop constraint FK_EVENEMEN_ASSOCIATI_UTILISAT;

alter table EVENEMENT
   drop constraint FK_EVENEMEN_ASSOCIATI_ETAT;

alter table TICKET
   drop constraint FK_TICKET_ACHAT_TIC_UTILISAT;

alter table TICKET
   drop constraint FK_TICKET_ASSOCIATI_EVENEMEN;

alter table TICKET
   drop constraint FK_TICKET_ASSOCIATI_CATEGORI;

alter table TICKET
   drop constraint FK_TICKET_ASSOCIATI_ETAT;

alter table UTILISATEUR
   drop constraint FK_UTILISAT_ASSOCIATI_ROLE;

alter table UTILISATEUR
   drop constraint FK_UTILISAT_ASSOCIATI_ETAT;

drop index ASSOCIATION_7_FK;

drop table CATEGORIE cascade constraints;

drop table ETAT cascade constraints;

drop index ASSOCIATION_6_FK;

drop index ASSOCIATION_4_FK;

drop table EVENEMENT cascade constraints;

drop table ROLE cascade constraints;

drop index ASSOCIATION_8_FK;

drop index ACHAT_TICKET_FK;

drop index ASSOCIATION_2_FK;

drop index ASSOCIATION_1_FK;

drop table TICKET cascade constraints;

drop index ASSOCIATION_9_FK;

drop index ASSOCIATION_5_FK;

drop table UTILISATEUR cascade constraints;

/*==============================================================*/
/* Table : CATEGORIE                                            */
/*==============================================================*/
create table CATEGORIE 
(
   IDCATEGORIE          INTEGER              not null,
   IDEVENEMENT          INTEGER              not null,
   LIBELLE              VARCHAR2(200),
   PRIX                 NUMBER,
   NOMBREBILLET         SMALLINT,
   constraint PK_CATEGORIE primary key (IDCATEGORIE)
);

/*==============================================================*/
/* Index : ASSOCIATION_7_FK                                     */
/*==============================================================*/
create index ASSOCIATION_7_FK on CATEGORIE (
   IDEVENEMENT ASC
);

/*==============================================================*/
/* Table : ETAT                                                 */
/*==============================================================*/
create table ETAT 
(
   IDETAT               INTEGER              not null,
   LIBELLE              VARCHAR2(200),
   constraint PK_ETAT primary key (IDETAT)
);

/*==============================================================*/
/* Table : EVENEMENT                                            */
/*==============================================================*/
create table EVENEMENT 
(
   IDEVENEMENT          INTEGER              not null,
   IDUSER               INTEGER              not null,
   IDETAT               INTEGER              not null,
   LIBELLE              VARCHAR2(200),
   DESCRIPTION          CLOB,
   DATEDEBUT            DATE,
   DATEFIN              DATE,
   NOMBREBILLET         SMALLINT,
   LIEU                 VARCHAR2(200),
   IMAGE                VARCHAR2(300),
   IMAGEBYTE			BLOB,
   constraint PK_EVENEMENT primary key (IDEVENEMENT)
);

/*==============================================================*/
/* Index : ASSOCIATION_4_FK                                     */
/*==============================================================*/
create index ASSOCIATION_4_FK on EVENEMENT (
   IDUSER ASC
);

/*==============================================================*/
/* Index : ASSOCIATION_6_FK                                     */
/*==============================================================*/
create index ASSOCIATION_6_FK on EVENEMENT (
   IDETAT ASC
);

/*==============================================================*/
/* Table : ROLE                                                 */
/*==============================================================*/
create table ROLE 
(
   IDROLE               INTEGER              not null,
   LIBELLE              VARCHAR2(200),
   constraint PK_ROLE primary key (IDROLE)
);

/*==============================================================*/
/* Table : TICKET                                               */
/*==============================================================*/
create table TICKET 
(
   IDTICKET             INTEGER              not null,
   IDEVENEMENT          INTEGER              not null,
   IDETAT               INTEGER              not null,
   IDUSER               INTEGER              not null,
   IDCATEGORIE          INTEGER              not null,
   NUMEROTICKET         VARCHAR2(100),
   constraint PK_TICKET primary key (IDTICKET)
);

/*==============================================================*/
/* Index : ASSOCIATION_1_FK                                     */
/*==============================================================*/
create index ASSOCIATION_1_FK on TICKET (
   IDEVENEMENT ASC
);

/*==============================================================*/
/* Index : ASSOCIATION_2_FK                                     */
/*==============================================================*/
create index ASSOCIATION_2_FK on TICKET (
   IDCATEGORIE ASC
);

/*==============================================================*/
/* Index : ACHAT_TICKET_FK                                      */
/*==============================================================*/
create index ACHAT_TICKET_FK on TICKET (
   IDUSER ASC
);

/*==============================================================*/
/* Index : ASSOCIATION_8_FK                                     */
/*==============================================================*/
create index ASSOCIATION_8_FK on TICKET (
   IDETAT ASC
);

/*==============================================================*/
/* Table : UTILISATEUR                                          */
/*==============================================================*/
create table UTILISATEUR 
(
   IDUSER               INTEGER              not null,
   IDETAT               INTEGER              not null,
   IDROLE               INTEGER              not null,
   NOM                  VARCHAR2(100),
   USERNAME             VARCHAR2(100),
   PASSWORD             VARCHAR2(100),
   constraint PK_UTILISATEUR primary key (IDUSER)
);

/*==============================================================*/
/* Index : ASSOCIATION_5_FK                                     */
/*==============================================================*/
create index ASSOCIATION_5_FK on UTILISATEUR (
   IDROLE ASC
);

/*==============================================================*/
/* Index : ASSOCIATION_9_FK                                     */
/*==============================================================*/
create index ASSOCIATION_9_FK on UTILISATEUR (
   IDETAT ASC
);

alter table CATEGORIE
   add constraint FK_CATEGORI_ASSOCIATI_EVENEMEN foreign key (IDEVENEMENT)
      references EVENEMENT (IDEVENEMENT);

alter table EVENEMENT
   add constraint FK_EVENEMEN_ASSOCIATI_UTILISAT foreign key (IDUSER)
      references UTILISATEUR (IDUSER);

alter table EVENEMENT
   add constraint FK_EVENEMEN_ASSOCIATI_ETAT foreign key (IDETAT)
      references ETAT (IDETAT);

alter table TICKET
   add constraint FK_TICKET_ACHAT_TIC_UTILISAT foreign key (IDUSER)
      references UTILISATEUR (IDUSER);

alter table TICKET
   add constraint FK_TICKET_ASSOCIATI_EVENEMEN foreign key (IDEVENEMENT)
      references EVENEMENT (IDEVENEMENT);

alter table TICKET
   add constraint FK_TICKET_ASSOCIATI_CATEGORI foreign key (IDCATEGORIE)
      references CATEGORIE (IDCATEGORIE);

alter table TICKET
   add constraint FK_TICKET_ASSOCIATI_ETAT foreign key (IDETAT)
      references ETAT (IDETAT);

alter table UTILISATEUR
   add constraint FK_UTILISAT_ASSOCIATI_ROLE foreign key (IDROLE)
      references ROLE (IDROLE);

alter table UTILISATEUR
   add constraint FK_UTILISAT_ASSOCIATI_ETAT foreign key (IDETAT)
      references ETAT (IDETAT);

